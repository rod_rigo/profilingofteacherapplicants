<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TeachersFixture
 */
class TeachersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'employment_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'email' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'answer' => ['type' => 'text', 'length' => 4294967295, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'first_name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'middle_name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'last_name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'birthday' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'age' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'civil_status_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sex_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'gender_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'contact_number' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'education_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'eligibility_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'position_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'barangay_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'religion_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ethinicity' => ['type' => 'text', 'length' => 4294967295, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'has_disability' => ['type' => 'tinyinteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'disability_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'employment_code' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'token' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_0900_ai_ci', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'modified' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'deleted' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'teachers to employments' => ['type' => 'index', 'columns' => ['employment_id'], 'length' => []],
            'teachers to civil_statuses' => ['type' => 'index', 'columns' => ['civil_status_id'], 'length' => []],
            'teachers to sexes' => ['type' => 'index', 'columns' => ['sex_id'], 'length' => []],
            'teachers to genders' => ['type' => 'index', 'columns' => ['gender_id'], 'length' => []],
            'teachers to educations' => ['type' => 'index', 'columns' => ['education_id'], 'length' => []],
            'teachers to eligibilities' => ['type' => 'index', 'columns' => ['eligibility_id'], 'length' => []],
            'teachers to positions' => ['type' => 'index', 'columns' => ['position_id'], 'length' => []],
            'teachers to barangays' => ['type' => 'index', 'columns' => ['barangay_id'], 'length' => []],
            'teachers to religions' => ['type' => 'index', 'columns' => ['religion_id'], 'length' => []],
            'teachers to disabilities' => ['type' => 'index', 'columns' => ['disability_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'teachers to barangays' => ['type' => 'foreign', 'columns' => ['barangay_id'], 'references' => ['barangays', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to civil_statuses' => ['type' => 'foreign', 'columns' => ['civil_status_id'], 'references' => ['civil_statuses', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to disabilities' => ['type' => 'foreign', 'columns' => ['disability_id'], 'references' => ['disabilities', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to educations' => ['type' => 'foreign', 'columns' => ['education_id'], 'references' => ['educations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to eligibilities' => ['type' => 'foreign', 'columns' => ['eligibility_id'], 'references' => ['eligibilities', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to employments' => ['type' => 'foreign', 'columns' => ['employment_id'], 'references' => ['employments', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to genders' => ['type' => 'foreign', 'columns' => ['gender_id'], 'references' => ['genders', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to positions' => ['type' => 'foreign', 'columns' => ['position_id'], 'references' => ['positions', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to religions' => ['type' => 'foreign', 'columns' => ['religion_id'], 'references' => ['religions', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'teachers to sexes' => ['type' => 'foreign', 'columns' => ['sex_id'], 'references' => ['sexes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_0900_ai_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'employment_id' => 1,
                'email' => 'Lorem ipsum dolor sit amet',
                'answer' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'first_name' => 'Lorem ipsum dolor sit amet',
                'middle_name' => 'Lorem ipsum dolor sit amet',
                'last_name' => 'Lorem ipsum dolor sit amet',
                'birthday' => '2024-02-28',
                'age' => 1,
                'civil_status_id' => 1,
                'sex_id' => 1,
                'gender_id' => 1,
                'contact_number' => 'Lorem ipsum dolor sit amet',
                'education_id' => 1,
                'eligibility_id' => 1,
                'position_id' => 1,
                'barangay_id' => 1,
                'religion_id' => 1,
                'ethinicity' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'has_disability' => 1,
                'disability_id' => 1,
                'employment_code' => 'Lorem ipsum dolor sit amet',
                'token' => 'Lorem ipsum dolor sit amet',
                'created' => 1709083467,
                'modified' => 1709083467,
                'deleted' => '2024-02-28 09:24:27',
            ],
        ];
        parent::init();
    }
}
