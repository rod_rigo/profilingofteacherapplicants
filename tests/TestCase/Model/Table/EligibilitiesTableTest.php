<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EligibilitiesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EligibilitiesTable Test Case
 */
class EligibilitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EligibilitiesTable
     */
    protected $Eligibilities;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Eligibilities',
        'app.Users',
        'app.Teachers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Eligibilities') ? [] : ['className' => EligibilitiesTable::class];
        $this->Eligibilities = $this->getTableLocator()->get('Eligibilities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Eligibilities);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\EligibilitiesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\EligibilitiesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
