<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TeachingTypesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TeachingTypesTable Test Case
 */
class TeachingTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TeachingTypesTable
     */
    protected $TeachingTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.TeachingTypes',
        'app.Users',
        'app.Employments',
        'app.Positions',
        'app.Teachers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('TeachingTypes') ? [] : ['className' => TeachingTypesTable::class];
        $this->TeachingTypes = $this->getTableLocator()->get('TeachingTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TeachingTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\TeachingTypesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\TeachingTypesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
