<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuffixesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuffixesTable Test Case
 */
class SuffixesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SuffixesTable
     */
    protected $Suffixes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Suffixes',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Suffixes') ? [] : ['className' => SuffixesTable::class];
        $this->Suffixes = $this->getTableLocator()->get('Suffixes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Suffixes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test query method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::query()
     */
    public function testQuery(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteAll method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::deleteAll()
     */
    public function testDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getSoftDeleteField method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::getSoftDeleteField()
     */
    public function testGetSoftDeleteField(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDelete method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::hardDelete()
     */
    public function testHardDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDeleteAll method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::hardDeleteAll()
     */
    public function testHardDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test restore method
     *
     * @return void
     * @uses \App\Model\Table\SuffixesTable::restore()
     */
    public function testRestore(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
