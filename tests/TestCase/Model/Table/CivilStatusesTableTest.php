<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CivilStatusesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CivilStatusesTable Test Case
 */
class CivilStatusesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CivilStatusesTable
     */
    protected $CivilStatuses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.CivilStatuses',
        'app.Users',
        'app.Teachers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('CivilStatuses') ? [] : ['className' => CivilStatusesTable::class];
        $this->CivilStatuses = $this->getTableLocator()->get('CivilStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CivilStatuses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CivilStatusesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\CivilStatusesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
