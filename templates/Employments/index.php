<?php
/**
 * @var \App\View\AppView $this
 */

?>

<?php if(!$employments->isEmpty()):?>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <?php foreach ($employments as $employment):?>
                    <div class="col-sm-12 col-md-12 col-lg-12 ftco-animate fadeInUp ftco-animated">
                        <div class="card my-2 py-4 px-5">
                            <div class="align-items-center">
                                <div class="text">
                                    <h4><?=strtoupper($employment->employment)?> | <span><?=strtoupper($employment->teaching_type->teaching_type)?></span></h4>
                                    <?=$employment->description?>
                                    <p>
                                        <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Teachers', 'action' => 'index', intval($employment->id)])?>" turbolink class="btn btn-primary">Apply</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>

<?php else:?>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="wrapper">
                        <div class="row no-gutters">
                            <div class="col-sm-12 col-lg-12 col-md-12 order-md-last d-flex align-items-stretch">
                                <div class="contact-wrap w-100 p-md-5 p-4">
                                    <h3 class="mb-4 text-center">
                                        No Application Available Please Check The Schedule
                                    </h3>

                                    <div class="media-body p-2 mt-3 d-flex justify-content-center align-items-center">
                                        <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Employment', 'action' => 'schedules'])?>" turbolink class="btn btn-secondary">
                                            Schedules
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif;?>


