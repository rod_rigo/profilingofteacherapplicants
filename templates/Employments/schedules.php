<?php
/**
 * @var \App\View\AppView $this
 */

?>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="wrapper">
                    <div class="row no-gutters">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?=$this->Html->script('employments/schedules')?>
