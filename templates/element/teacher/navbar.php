<?php
/**
 * @var \App\View\AppView $this
 */?>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="javascript:void(0);">
            Teaching & Non-Teaching<span style="color: #fece09;"> Application</span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="flaticon-menu"></i> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav m-auto">
                <li class="nav-item <?=((strtolower($controller) === strtolower('employment') && strtolower($action) == strtolower('index')) || (strtolower($controller) === strtolower('teachers')))? 'active': null;?>">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Employment', 'action' => 'index'])?>" turbolink class="nav-link">Registration</a>
                </li>
                <li class="nav-item <?=(strtolower($controller) === strtolower('employment') && strtolower($action) == strtolower('schedules'))? 'active': null;?>">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Employment', 'action' => 'schedules'])?>" turbolink class="nav-link">Schedules</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
