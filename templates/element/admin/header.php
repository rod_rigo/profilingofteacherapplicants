<?php
/**
 * @var \App\View\AppView
 */
?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <?=strtoupper($controller)?>
                    </li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
