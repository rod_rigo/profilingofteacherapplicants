<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
        <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle w-100" loading="lazy" title="Deped Logo" alt="DEPED">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Navigation</li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('dashboards'))? 'menu-is-opening menu-open': null;?>">
                    <a href="#" class="nav-link <?=(strtolower($controller) == strtolower('dashboards'))? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <?php foreach ($sidebars as $sidebar):?>
                            <li class="nav-item" title="<?=strtoupper($sidebar->teaching_type)?>">
                                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index', intval($sidebar->id)])?>" class="nav-link <?=(strtolower('dashboards') == strtolower($controller) && strtolower('index') == strtolower($action) && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null?>" turbolink>
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        <?=$this->Text->truncate(strtoupper($sidebar->teaching_type),16,[
                                            'ellipsis' => '...',
                                            'exact' => true
                                        ])?>
                                    </p>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </li>
                <?php foreach ($sidebars as $sidebar):?>
                    <li class="nav-item <?=(strtolower($controller) == strtolower('teachers') && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'menu-is-opening menu-open': null;?>" title="<?=strtoupper($sidebar->teaching_type)?>">
                        <a href="#" class="nav-link <?=(strtolower($controller) == strtolower('teachers') && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null;?>">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                <?=$this->Text->truncate(strtoupper($sidebar->teaching_type),16,[
                                    'ellipsis' => '...',
                                    'exact' => true
                                ])?>
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'index', intval($sidebar->id)])?>" class="nav-link <?=(strtolower('teachers') == strtolower($controller) && strtolower('index') == strtolower($action) && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null?>" turbolink>
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>All</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'today', intval($sidebar->id)])?>" class="nav-link <?=(strtolower('teachers') == strtolower($controller) && strtolower('today') == strtolower($action) && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null?>" turbolink>
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Today</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'week', intval($sidebar->id)])?>" class="nav-link <?=(strtolower('teachers') == strtolower($controller) && strtolower('week') == strtolower($action) && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null?>" turbolink>
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Week</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'month', intval($sidebar->id)])?>" class="nav-link <?=(strtolower('teachers') == strtolower($controller) && strtolower('month') == strtolower($action) && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null?>" turbolink>
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Month</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'year', intval($sidebar->id)])?>" class="nav-link <?=(strtolower('teachers') == strtolower($controller) && strtolower('year') == strtolower($action) && intval($this->request->getParam('pass.0')) == intval($sidebar->id))? 'active': null?>" turbolink>
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Year</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endforeach;?>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'vacancy'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('teachers') && strtolower('bin') != strtolower($action) && strtolower('vacancy') == strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Teachers
                        </p>
                    </a>
                </li>
                <li class="nav-header">Components</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Barangays', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('barangays') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-map-marked"></i>
                        <p>
                            Barangays
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'CivilStatuses', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('civilstatuses') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Civil Statuses
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Disabilities', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('disabilities') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-alt-slash"></i>
                        <p>
                            Disabilities
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Genders', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('genders') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-male"></i>
                        <p>
                            Genders
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Sexes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('sexes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-female"></i>
                        <p>
                            Sexes
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Suffixes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('suffixes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Suffixes
                        </p>
                    </a>
                </li>

                <li class="nav-header">Configuration</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('users') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Educations', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('educations') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-graduate"></i>
                        <p>
                            Educations
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Eligibilities', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('eligibilities') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-check"></i>
                        <p>
                            Eligibilities
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('positions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            Positions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Religions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('religions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Religions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'TeachingTypes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('teachingtypes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-plus"></i>
                        <p>
                            Teaching-Types
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Employments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('employments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-md"></i>
                        <p>
                            Activities
                        </p>
                    </a>
                </li>

                <li class="nav-item <?=(strtolower($action) == strtolower('bin'))? 'menu-is-opening menu-open': null;?>">
                    <a href="#" class="nav-link <?=(strtolower($action) == strtolower('bin'))? 'active': null;?>">
                        <i class="nav-icon fas fa-trash"></i>
                        <p>
                            Bin
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Barangays', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('barangays') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Barangays</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'CivilStatuses', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('civilstatuses') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Civil Statuses</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Disabilities', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('disabilities') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Disabilities</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Educations', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('educations') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Educations</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Eligibilities', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('eligibilities') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Eligibilities</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Genders', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('genders') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Genders</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Sexes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('sexes') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sexes</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('positions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Positions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Religions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('religions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Religions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Employments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('employments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Activities</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'TeachingTypes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('TeachingTypes') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Teaching-Types</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Suffixes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('suffixes') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Suffixes</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
