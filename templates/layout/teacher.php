<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = ucwords('RSP');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> |
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('deped.png','/deped.png',['type'=>'icon']); ?>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">
    <?= $this->Html->meta('csrf-token',$this->request->getAttribute('csrfToken')) ?>

    <?=$this->Html->css([
        '/jquery/css/jquery-ui',
        '/i-check/css/icheck-bootstrap',
        '/life-coach/css/animate',
        '/life-coach/css/owl.carousel.min',
        '/life-coach/css/owl.theme.default.min',
        '/life-coach/css/magnific-popup.css',
        '/life-coach/css/flaticon',
        '/life-coach/css/style',
        '/select2/css/select2',
        '/evo-calendar/css/evo-calendar',
        '/evo-calendar/css/evo-calendar.orange-coral',
        'teacher'
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/chartjs/js/chart',
        '/ck-editor/js/ckeditor',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2',
        '/turbo-links/js/turbolinks',
        '/evo-calendar/js/evo-calendar',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/sdo-santiago-RSP/';
    </script>

</head>
<body>

<?=$this->element('teacher/header')?>

<?=$this->element('teacher/navbar')?>

<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>

<?=$this->element('teacher/footer')?>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

<?=$this->Html->script([
    '/life-coach/js/jquery-migrate-3.0.1.min',
    '/life-coach/js/popper.min',
    '/life-coach/js/bootstrap.min',
    '/life-coach/js/jquery.easing.1.3',
    '/life-coach/js/jquery.waypoints.min',
    '/life-coach/js/jquery.stellar.min',
    '/life-coach/js/jquery.animateNumber.min',
    '/life-coach/js/owl.carousel.min',
    '/life-coach/js/jquery.magnific-popup.min',
    '/life-coach/js/scrollax.min',
    '/life-coach/js/main',
    '/select2/js/select2',
    'teacher'
])?>
</body>
</html>
