<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?php if(!empty($employment)):?>
    <?=$this->Html->css('teachers/index')?>
        <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="wrapper">
                        <div class="row no-gutters">
                            <div class="col-sm-12 col-lg-12 col-md-12 order-md-last d-flex align-items-stretch">
                                <div class="contact-wrap w-100 p-md-5 p-4">
                                    <h3 class="mb-4">
                                        <?=ucwords($employment->employment)?>
                                    </h3>
                                    <div id="form-message-warning" class="mb-4"></div>

                                    <?=$this->Form->create($teacher,['id' => 'form', 'type' => 'file', 'class' => 'contactForm'])?>
                                    <div class="row">

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div id="form-message-success" class="mb-4 text-justify text-dark text-capitalize">
                                                <?=($employment->description)?>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('email', ucwords('email ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->email('email',[
                                                    'class' => 'form-control',
                                                    'id' => 'email',
                                                    'required' => true,
                                                    'title' => ucwords('Please Enter @ In Email'),
                                                    'placeholder' => ucwords('Enter Your Email'),
                                                    'pattern' => '[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('first_name', ucwords('first name ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->text('first_name',[
                                                    'class' => 'form-control',
                                                    'id' => 'first-name',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Enter Your First Name'),
                                                    'pattern' => '(.){1,}',
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('middle_name', ucwords('Middle name ').'<span class="text-info">* (Optional)</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->text('middle_name',[
                                                    'class' => 'form-control',
                                                    'id' => 'middle-name',
                                                    'required' => false,
                                                    'title' => ucwords('this field is optional'),
                                                    'placeholder' => ucwords('Enter Your Middle Name (Optional)'),
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('last_name', ucwords('Last name ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->text('last_name',[
                                                    'class' => 'form-control',
                                                    'id' => 'last-name',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Enter Your Last Name'),
                                                    'pattern' => '(.){1,}',
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('suffix_id', ucwords('Suffix ').'<span class="text-info">* (Optional)</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->select('suffix_id', $suffixes,[
                                                    'class' => 'form-control',
                                                    'id' => 'suffix-id',
                                                    'required' => true,
                                                    'title' => ucwords('this field is optional'),
                                                    'empty' => false
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-9 col-lg-9 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('birthday', ucwords('Birthday ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->date('birthday',[
                                                    'class' => 'form-control',
                                                    'id' => 'birthday',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('age', ucwords('Age ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->number('age',[
                                                    'class' => 'form-control',
                                                    'id' => 'age',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Enter Your Age'),
                                                    'pattern' => '(.){1,}',
                                                    'min' => 18
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?=$this->Form->label('civil_status_id', ucwords('civil status ').'<span class="text-danger">*</span>',[
                                                'class' => 'label h6',
                                                'escape' => false
                                            ])?>
                                            <div class="form-group d-flex flex-column justify-content-center align-items-start">
                                                <?php foreach ($civilStatuses as $key => $value):?>
                                                    <div class="icheck-primary d-inline my-2">
                                                        <?=$this->Form->checkbox('civil_status.'.($key),[
                                                            'id' => 'civil-status-'.($key),
                                                            'label' => false,
                                                            'hiddenField' => false,
                                                            'checked' => false,
                                                            'value' => intval($key),
                                                            'required' => true,
                                                            'class' => 'civil-status'
                                                        ])?>
                                                        <?=$this->Form->label('civil_status.'.($key), ucwords($value),[
                                                            'class' => 'text-dark'
                                                        ])?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                            <?=$this->Form->hidden('civil_status_id',[
                                                'id' => 'civil-status-id',
                                                'required' => true
                                            ])?>
                                            <small></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?=$this->Form->label('sex_id', ucwords('Sex ').'<span class="text-danger">*</span>',[
                                                'class' => 'label h6',
                                                'escape' => false
                                            ])?>
                                            <div class="form-group d-flex flex-column justify-content-center align-items-start">
                                                <?php foreach ($sexes as $key => $value):?>
                                                    <div class="icheck-primary d-inline my-2">
                                                        <?=$this->Form->checkbox('sex.'.($key),[
                                                            'id' => 'sex-'.($key),
                                                            'label' => false,
                                                            'hiddenField' => false,
                                                            'checked' => false,
                                                            'value' => intval($key),
                                                            'required' => true,
                                                            'class' => 'sex'
                                                        ])?>
                                                        <?=$this->Form->label('sex.'.($key), ucwords($value),[
                                                            'class' => 'text-dark'
                                                        ])?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                            <?=$this->Form->hidden('sex_id',[
                                                'id' => 'sex-id',
                                                'required' => true
                                            ])?>
                                            <small></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?=$this->Form->label('gender_id', ucwords('Gender ').'<span class="text-danger">*</span>',[
                                                'class' => 'label h6',
                                                'escape' => false
                                            ])?>
                                            <div class="form-group d-flex flex-column justify-content-center align-items-start">
                                                <?php foreach ($genders as $key => $value):?>
                                                    <div class="icheck-primary d-inline my-2">
                                                        <?=$this->Form->checkbox('gender.'.($key),[
                                                            'id' => 'gender-'.($key),
                                                            'label' => false,
                                                            'hiddenField' => false,
                                                            'checked' => false,
                                                            'value' => intval($key),
                                                            'required' => true,
                                                            'class' => 'gender'
                                                        ])?>
                                                        <?=$this->Form->label('gender.'.($key), ucwords($value),[
                                                            'class' => 'text-dark'
                                                        ])?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                            <?=$this->Form->hidden('gender_id',[
                                                'id' => 'gender-id',
                                                'required' => true
                                            ])?>
                                            <small></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('contact_number', ucwords('Contact Number ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->text('contact_number',[
                                                    'class' => 'form-control',
                                                    'id' => 'contact-number',
                                                    'required' => true,
                                                    'title' => ucwords('Contact number must be start with  0 & 9 followed by 9 digits'),
                                                    'placeholder' => ucwords('Enter Your Contact Number'),
                                                    'pattern' => '(09)([0-9]{9})',
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('education', ucwords('Education ').'<span class="text-danger">*</span> (Please Do Not Use Abbreviation)',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->text('education',[
                                                    'class' => 'form-control',
                                                    'id' => 'education',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Enter You Education')
                                                ])?>
                                                <small></small>
                                            </div>
                                            <?=$this->Form->hidden('education_id',[
                                                'class' => 'form-control',
                                                'id' => 'education-id',
                                                'required' => true,
                                                'title' => ucwords('please fill out this field'),
                                                'value' => intval(@$educations->id)
                                            ])?>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?=$this->Form->label('eligibility_id', ucwords('Eligibility ').'<span class="text-danger">*</span>',[
                                                'class' => 'label h6',
                                                'escape' => false
                                            ])?>
                                            <div class="form-group d-flex flex-column justify-content-center align-items-start">
                                                <?php foreach ($eligibilities as $key => $value):?>
                                                    <div class="icheck-primary d-inline my-2">
                                                        <?=$this->Form->checkbox('eligibility.'.($key),[
                                                            'id' => 'eligibility-'.($key),
                                                            'label' => false,
                                                            'hiddenField' => false,
                                                            'checked' => false,
                                                            'value' => intval($key),
                                                            'required' => true,
                                                            'class' => 'eligibility'
                                                        ])?>
                                                        <?=$this->Form->label('eligibility.'.($key), ucwords($value),[
                                                            'class' => 'text-dark'
                                                        ])?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                            <?=$this->Form->hidden('eligibility_id',[
                                                'id' => 'eligibility-id',
                                                'required' => true
                                            ])?>
                                            <small></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('position_id', ucwords('Level Of Position Applied For ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->select('position_id', $positions,[
                                                    'class' => 'form-control',
                                                    'id' => 'position-id',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'empty' => ucwords('Choose Applicant')
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <?php if(!boolval($employment->teaching_type->is_non_teaching)):?>
                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                                <div class="form-group">
                                                    <?=$this->Form->label('barangay_id', ucwords('Barangay Residence ').'<span class="text-danger">*</span>',[
                                                        'class' => 'label',
                                                        'escape' => false
                                                    ])?>
                                                    <?=$this->Form->select('barangay_id', $barangays,[
                                                        'class' => 'form-control',
                                                        'id' => 'barangay-id',
                                                        'required' => true,
                                                        'title' => ucwords('please fill out this field'),
                                                        'empty' => ucwords('Select Barangay Residence')
                                                    ])?>
                                                </div>
                                                <small id="small-barangay-id"></small>
                                            </div>

                                            <?=$this->Form->hidden('address',[
                                                'class' => 'form-control',
                                                'id' => 'address',
                                                'required' => true,
                                                'title' => ucwords('please fill out this field'),
                                                'placeholder' => ucwords('Enter address'),
                                                'pattern' => '(.){1,}',
                                                'value' => '-'
                                            ])?>
                                        <?php else:?>
                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                                <div class="form-group">
                                                    <?=$this->Form->label('barangay_id', ucwords('Barangay Residence ').'<span class="text-danger">*</span>',[
                                                        'class' => 'label',
                                                        'escape' => false
                                                    ])?>
                                                    <?=$this->Form->select('barangay_id', $barangays,[
                                                        'class' => 'form-control',
                                                        'id' => 'barangay-id',
                                                        'required' => true,
                                                        'title' => ucwords('please fill out this field'),
                                                        'empty' => false
                                                    ])?>
                                                </div>
                                                <small id="small-barangay-id"></small>
                                            </div>

                                            <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                                <div class="form-group">
                                                    <?=$this->Form->label('address', ucwords('address ').'<span class="text-danger">*</span>',[
                                                        'class' => 'label',
                                                        'escape' => false
                                                    ])?>
                                                    <?=$this->Form->text('address',[
                                                        'class' => 'form-control',
                                                        'id' => 'address',
                                                        'required' => true,
                                                        'title' => ucwords('please fill out this field'),
                                                        'placeholder' => ucwords('Enter address'),
                                                        'pattern' => '(.){1,}',
                                                    ])?>
                                                    <small></small>
                                                </div>
                                            </div>
                                        <?php endif;?>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('religion_id', ucwords('Religion ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->select('religion_id', $religions,[
                                                    'class' => 'form-control',
                                                    'id' => 'religion-id',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'empty' => ucwords('Select Religion')
                                                ])?>
                                            </div>
                                            <small id="small-religion-id"></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->label('ethinicity', ucwords('ethinicity ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->textarea('ethinicity',[
                                                    'class' => 'form-control',
                                                    'id' => 'ethinicity',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Enter Your Answer'),
                                                    'pattern' => '(.){1,}',
                                                    'style' => 'resize:none;'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?=$this->Form->label('has_disability', ucwords('With Disability ').'<span class="text-danger">*</span>',[
                                                'class' => 'label h6',
                                                'escape' => false
                                            ])?>
                                            <div class="form-group d-flex flex-column justify-content-center align-items-start">
                                                <?php $hasDisabilities = [1 => ucwords('Yes'), 0 => ucwords('No')];?>
                                                <?php foreach ($hasDisabilities as $key => $value):?>
                                                    <div class="icheck-primary d-inline my-2">
                                                        <?=$this->Form->checkbox('has_disability.'.($key),[
                                                            'id' => 'has-disability-'.($key),
                                                            'label' => false,
                                                            'hiddenField' => false,
                                                            'checked' => false,
                                                            'value' => intval($key),
                                                            'required' => true,
                                                            'class' => 'has-disability'
                                                        ])?>
                                                        <?=$this->Form->label('has_disability.'.($key), ucwords($value),[
                                                            'class' => 'text-dark'
                                                        ])?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                            <?=$this->Form->hidden('has_disability',[
                                                'id' => 'has-disability',
                                                'required' => true
                                            ])?>
                                            <small></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?=$this->Form->label('disability_id', ucwords('Type Of Disability ').'<span class="text-danger">*</span>',[
                                                'class' => 'label h6',
                                                'escape' => false
                                            ])?>
                                            <div class="form-group d-flex flex-column justify-content-center align-items-start">
                                                <?php foreach ($disabilities as $key => $disability):?>
                                                    <div class="icheck-primary d-inline my-2">
                                                        <?=$this->Form->checkbox('disability.'.($key),[
                                                            'id' => 'disability-'.($key),
                                                            'label' => false,
                                                            'hiddenField' => false,
                                                            'checked' => false,
                                                            'value' => intval($disability->id),
                                                            'required' => true,
                                                            'class' => 'disability',
                                                            'data-target' => intval($disability->is_no)
                                                        ])?>
                                                        <?=$this->Form->label('disability.'.($key), ucwords($disability->disability),[
                                                            'class' => 'text-dark'
                                                        ])?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                            <?=$this->Form->hidden('disability_id',[
                                                'id' => 'disability-id',
                                                'required' => true
                                            ])?>
                                            <small></small>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div id="form-message-success" class="mb-1 text-justify text-dark text-capitalize">
                                                <strong>
                                                    How did you hear about the position?
                                                </strong>
                                            </div>
                                            <div class="form-group">
                                                <?=$this->Form->label('answer', ucwords('answer ').'<span class="text-danger">*</span>',[
                                                    'class' => 'label',
                                                    'escape' => false
                                                ])?>
                                                <?=$this->Form->textarea('answer',[
                                                    'class' => 'form-control',
                                                    'id' => 'answer',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Enter Your Answer'),
                                                    'pattern' => '(.){1,}',
                                                    'style' => 'resize:none;',
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <div class="form-group">
                                                <?=$this->Form->hidden('token',[
                                                    'id' => 'token',
                                                    'token' => true,
                                                    'value' => uniqid()
                                                ])?>
                                                <?=$this->Form->hidden('no',[
                                                    'id' => 'no',
                                                    'required' => true,
                                                    'value' => intval(0)
                                                ])?>
                                                <?=$this->Form->hidden('code',[
                                                    'id' => 'code',
                                                    'required' => true,
                                                    'value' => strval(uniqid())
                                                ])?>
                                                <?=$this->Form->hidden('employment_code',[
                                                    'id' => 'employment-code',
                                                    'required' => true,
                                                    'value' => uniqid()
                                                ])?>
                                                <?=$this->Form->hidden('is_non_teaching',[
                                                    'id' => 'is-non-teaching',
                                                    'required' => true,
                                                    'value' => intval($employment->teaching_type->is_non_teaching)
                                                ])?>
                                                <?=$this->Form->hidden('is_transfered',[
                                                    'id' => 'is-transfered',
                                                    'required' => true,
                                                    'value' => intval(0)
                                                ])?>
                                                <?=$this->Form->hidden('is_occupied',[
                                                    'id' => 'is-occupied',
                                                    'required' => true,
                                                    'value' => intval(0)
                                                ])?>
                                                <?=$this->Form->hidden('employment_id',[
                                                    'id' => 'employment-id',
                                                    'required' => true,
                                                    'value' => intval($employment->id)
                                                ])?>
                                                <?=$this->Form->hidden('teaching_type_id',[
                                                    'id' => 'teaching-type-id',
                                                    'required' => true,
                                                    'value' => intval($employment->teaching_type_id)
                                                ])?>
                                                <?=$this->Form->button('Submit',[
                                                    'class' => 'btn btn-success btn-lg float-right'
                                                ])?>
                                            </div>
                                        </div>

                                    </div>
                                    <?=$this->Form->end()?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?=$this->Html->script('teachers/index')?>
<?php else:?>
    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="wrapper">
                        <div class="row no-gutters">
                            <div class="col-sm-12 col-lg-12 col-md-12 order-md-last d-flex align-items-stretch">
                                <div class="contact-wrap w-100 p-md-5 p-4">
                                    <h3 class="mb-4 text-center">
                                        No Application Available Please Check The Schedule
                                    </h3>

                                    <div class="media-body p-2 mt-3 d-flex justify-content-center align-items-center">
                                        <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Employment', 'action' => 'schedules'])?>" turbolink class="btn btn-secondary">
                                            Schedules
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;?>



