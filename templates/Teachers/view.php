<?php
/**
 * @var \App\View\AppView $this
 */?>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="wrapper">
                    <div class="row no-gutters">
                        <div class="col-sm-12 col-lg-12 col-md-12 order-md-last d-flex align-items-stretch">
                            <div class="contact-wrap w-100 p-md-5 p-4">
                                <h3 class="mb-4 text-center">
                                    <?=ucwords($teacher->employment->employment)?>
                                </h3>

                                <div id="form-message-warning" class="mb-4"></div>

                                <div class="media-body p-2 mt-3 d-flex justify-content-center align-items-center">
                                    <img src="<?=$this->Url->assetUrl('/img/check.png')?>" style="object-fit: contain;" alt="Check Image" loading="lazy">
                                </div>
                                <div class="media-body p-2 mt-3">
                                    <h3 class="heading text-center">You Have Been Successfully Registered</h3>
                                    <h4 class="heading text-center">Your Application Code Is <strong><?=$teacher->employment_code?></strong></h4>
                                </div>

                                <div class="media-body p-2 mt-3 d-flex justify-content-center align-items-center">
                                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Teachers', 'action' => 'edit', base64_encode(strval($teacher->id))])?>" turbolink class="btn btn-secondary">
                                        Update Registration
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

