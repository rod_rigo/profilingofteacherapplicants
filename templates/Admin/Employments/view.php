<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employment $employment
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<script>
    var id = parseInt(<?=intval($employment->id)?>);
</script>

<?=$this->Html->css('admin/employments/view')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Activity Form</h3>
            </div>
            <?= $this->Form->create($employment,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('teaching_type_id', ucwords('Teaching-Type'))?>
                        <?=$this->Form->select('teaching_type_id', $teachingTypes,[
                            'class' => 'form-control',
                            'id' => 'teaching-type-id',
                            'required' => true,
                            'empty' => ucwords('Select Teaching-Type'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('employment', ucwords('Activity'))?>
                        <?= $this->Form->text('employment',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Activity'),
                            'id' => 'employment',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-2">
                        <?=$this->Form->label('year', ucwords('year'))?>
                        <?= $this->Form->year('year',[
                            'class' => 'form-control',
                            'id' => 'year',
                            'title' => ucwords('please fill out this field'),
                            'min' => 2000,
                            'value' => date('Y')
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                        <?= $this->Form->date('start_date',[
                            'class' => 'form-control',
                            'id' => 'start-date',
                            'title' => ucwords('please fill out this field'),
                            'required' => true,
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                        <?=$this->Form->label('end_date', ucwords('end Date'))?>
                        <?= $this->Form->date('end_date',[
                            'class' => 'form-control',
                            'id' => 'end-date',
                            'title' => ucwords('please fill out this field'),
                            'required' => true,
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('per_year',[
                                'id' => 'per-year',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($employment->is_per_year),
                            ])?>
                            <?=$this->Form->label('per_year', ucwords('Per Year'))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('description', ucwords('description'))?>
                        <?= $this->Form->textarea('description',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('description'),
                            'id' => 'description',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($employment->is_active),
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => intval(@$auth['id'])
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($employment->is_active),
                    'required' => true,
                ]);?>
                <?= $this->Form->hidden('is_per_year',[
                    'id' => 'is-per-year',
                    'value' => intval($employment->is_per_year),
                    'required' => true,
                ]);?>
                <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Employments', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit'),
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/employments/view')?>
