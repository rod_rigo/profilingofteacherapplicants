<?php
/**
 * @var \App\View\AppView $this
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$column = 'A';

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

foreach ($data as $key => $row){

    $count = intval(1);

    $title = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, strtoupper($row['title']));
    $spreadsheet->addSheet($title, $key);

    $sheet = $spreadsheet->setActiveSheetIndex($key);

    foreach ($row['rows'] as $value){
        $sheet
            ->setCellValue('A'.(strval($count)), strtoupper($value['label']))
            ->getStyle('A'.(strval($count)))
            ->getFont()
            ->setSize(14)
            ->setBold(false);

        $sheet
            ->setCellValue('B'.(strval($count)), doubleval($value['total']))
            ->getStyle('B'.(strval($count)))
            ->getFont()
            ->setSize(14)
            ->setBold(false);

        $count++;
    }

    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(intval(200), 'pt');
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(intval(200), 'pt');

}

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', ucwords($filename))) . '.xlsx') . '"');
$writer->save('php://output');
exit(0);