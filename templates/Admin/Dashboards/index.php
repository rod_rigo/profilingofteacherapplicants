<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    var id = parseInt(<?=intval($teachingType->id)?>);
    var is_non_teaching = parseInt(<?=intval($teachingType->is_non_teaching)?>);
    var teachingType = ('<?=strtoupper($teachingType->teaching_type)?>').toUpperCase();
</script>

<div class="row">
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-user"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Today</span>
                <span class="info-box-number" id="total-teachers-today">0</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-user"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Week</span>
                <span class="info-box-number" id="total-teachers-week">0</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-user"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Month</span>
                <span class="info-box-number" id="total-teachers-month">0</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-user"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Year</span>
                <span class="info-box-number" id="total-teachers-year">0</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>

<?=$this->Form->create(null,['id' => 'form', 'type' => 'file', 'class' => 'row']);?>
<div class="col-sm-12 col-md-3 col-lg-3 mt-3">
    <?=$this->Form->label('start_date', ucwords('start date'));?>
    <?=$this->Form->date('start_date',[
        'class' => 'form-control rounded-0',
        'id' => 'start-date',
        'required' => true,
        'title' => ucwords('pleas fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
    ])?>
</div>
<div class="col-sm-12 col-md-3 col-lg-3 mt-3">
    <?=$this->Form->label('end_date', ucwords('end date'));?>
    <?=$this->Form->date('end_date',[
        'class' => 'form-control rounded-0',
        'id' => 'end-date',
        'required' => true,
        'title' => ucwords('pleas fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
    ])?>
</div>
<div class="col-sm-12 col-md-4 col-lg-4 mt-3">
    <?=$this->Form->label('employment_id', ucwords('Activity'));?>
    <?=$this->Form->select('employment_id', $employments,[
        'class' => 'form-control rounded-0',
        'id' => 'employment-id',
        'required' => true,
        'title' => ucwords('pleas fill out this field'),
        'empty' => ucwords('Employment')
    ])?>
</div>
<div class="col-sm-12 col-md-2 col-lg-2 mt-3 d-flex justify-content-start align-items-end">
    <?=$this->Form->button(ucwords('search'),[
        'class' => 'btn btn-primary rounded-0',
        'type' => 'submit'
    ])?>
    <?=$this->Form->button(ucwords('XLSX'),[
        'class' => 'btn btn-success rounded-0',
        'type' => 'button',
        'id' => 'xlsx'
    ])?>
</div>
<?=$this->Form->end();?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-content-center my-2">
                    <button class="btn btn-info btn-border btn-round btn-sm mr-2 png" data-target="teacher-chart" data-title="Applicants">
                    <span class="btn-label">
                        <i class="fas fa-file-image"></i>
                    </span>
                        PNG
                    </button>
                    <button class="btn btn-success btn-border btn-round btn-sm mr-2 excel" data-target="teacher-chart" data-title="Applicants">
                    <span class="btn-label">
                        <i class="fas fa-file-excel"></i>
                    </span>
                        CSV
                    </button>
                    <button class="btn btn-secondary btn-border btn-round btn-sm print" data-target="teacher-chart" data-title="Applicants">
                    <span class="btn-label">
                        <i class="fa fa-print"></i>
                    </span>
                        Print
                    </button>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 my-2" style="height: 40em !important;">
                    <canvas id="teacher-chart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
        <div class="card p-3">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-content-center my-2">
                    <button class="btn btn-info btn-border btn-round btn-sm mr-2 png" data-target="gender-chart" data-title="Gender">
                    <span class="btn-label">
                        <i class="fas fa-file-image"></i>
                    </span>
                        PNG
                    </button>
                    <button class="btn btn-success btn-border btn-round btn-sm mr-2 excel" data-target="gender-chart" data-title="Gender">
                    <span class="btn-label">
                        <i class="fas fa-file-excel"></i>
                    </span>
                        CSV
                    </button>
                    <button class="btn btn-secondary btn-border btn-round btn-sm print" data-target="gender-chart" data-title="Gender">
                    <span class="btn-label">
                        <i class="fa fa-print"></i>
                    </span>
                        Print
                    </button>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 my-2" style="height: 40em !important;">
                    <canvas id="gender-chart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
        <div class="card p-3">
           <div class="row">
               <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-content-center my-2">
                   <button class="btn btn-info btn-border btn-round btn-sm mr-2 png" data-target="civil-status-chart" data-title="Civil Status">
                    <span class="btn-label">
                        <i class="fas fa-file-image"></i>
                    </span>
                       PNG
                   </button>
                   <button class="btn btn-success btn-border btn-round btn-sm mr-2 excel" data-target="civil-status-chart" data-title="Civil Status">
                    <span class="btn-label">
                        <i class="fas fa-file-excel"></i>
                    </span>
                       CSV
                   </button>
                   <button class="btn btn-secondary btn-border btn-round btn-sm print" data-target="civil-status-chart" data-title="Civil Status">
                    <span class="btn-label">
                        <i class="fa fa-print"></i>
                    </span>
                       Print
                   </button>
               </div>
               <div class="col-sm-12 col-md-12 col-lg-12 my-2" style="height: 40em !important;">
                   <canvas id="civil-status-chart" width="400" height="400"></canvas>
               </div>
           </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-content-center my-2">
                    <button class="btn btn-info btn-border btn-round btn-sm mr-2 png" data-target="religion-chart" data-title="Religion">
                    <span class="btn-label">
                        <i class="fas fa-file-image"></i>
                    </span>
                        PNG
                    </button>
                    <button class="btn btn-success btn-border btn-round btn-sm mr-2 excel" data-target="religion-chart" data-title="Religion">
                    <span class="btn-label">
                        <i class="fas fa-file-excel"></i>
                    </span>
                        CSV
                    </button>
                    <button class="btn btn-secondary btn-border btn-round btn-sm print" data-target="religion-chart" data-title="Religion">
                    <span class="btn-label">
                        <i class="fa fa-print"></i>
                    </span>
                        Print
                    </button>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 my-2" style="height: 40em !important;">
                    <canvas id="religion-chart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3" style="height: 40em !important;">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-content-center my-2">
                    <button class="btn btn-info btn-border btn-round btn-sm mr-2 png" data-target="education-chart" data-title="Education">
                    <span class="btn-label">
                        <i class="fas fa-file-image"></i>
                    </span>
                        PNG
                    </button>
                    <button class="btn btn-success btn-border btn-round btn-sm mr-2 excel" data-target="education-chart" data-title="Education">
                    <span class="btn-label">
                        <i class="fas fa-file-excel"></i>
                    </span>
                        CSV
                    </button>
                    <button class="btn btn-secondary btn-border btn-round btn-sm print" data-target="education-chart" data-title="Education">
                    <span class="btn-label">
                        <i class="fa fa-print"></i>
                    </span>
                        Print
                    </button>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 my-2" style="height: 40em !important;">
                    <canvas id="education-chart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!boolval($teachingType->is_non_teaching)):?>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card p-3">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-content-center my-2">
                        <button class="btn btn-info btn-border btn-round btn-sm mr-2 png" data-target="barangay-chart" data-title="Barangay">
                    <span class="btn-label">
                        <i class="fas fa-file-image"></i>
                    </span>
                            PNG
                        </button>
                        <button class="btn btn-success btn-border btn-round btn-sm mr-2 excel" data-target="barangay-chart" data-title="Barangay">
                    <span class="btn-label">
                        <i class="fas fa-file-excel"></i>
                    </span>
                            CSV
                        </button>
                        <button class="btn btn-secondary btn-border btn-round btn-sm print" data-target="barangay-chart" data-title="Barangay">
                    <span class="btn-label">
                        <i class="fa fa-print"></i>
                    </span>
                            Print
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 my-2" style="height: 40em !important;">
                        <canvas id="barangay-chart" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>



<?=$this->Html->script('admin/dashboards/index')?>


