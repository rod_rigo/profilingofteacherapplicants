<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher|\Cake\Collection\CollectionInterface $teacher
 * @var \App\Model\Entity\Employment|\Cake\Collection\CollectionInterface $employment
 */
?>

<script>
    var address = '<?=(strval($teacher['address']))?>';
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($entity,['type' => 'file', 'id' => 'form', 'class' => 'card card-info']) ?>
        <div class="card-header">
            <h3 class="card-title">Teacher Transfer Form - <?=strtoupper($teacher['first_name']).' '.strtoupper($teacher['last_name'])?></h3>
        </div>

        <div class="card-body">
            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <?=$this->Form->label('teaching_type_id', ucwords('Teaching Type').'<span class="text-danger">*</span>',[
                        'class' => 'label',
                        'escape' => false
                    ])?>
                    <?=$this->Form->select('teaching_type_id', $teachingTypes,[
                        'class' => 'form-control',
                        'id' => 'teaching-type-id',
                        'required' => true,
                        'title' => ucwords('please fill out this field'),
                        'empty' => ucwords('Select Employment')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 mt-3">
                    <?=$this->Form->label('employment_id', ucwords('Employment').'<span class="text-danger">*</span>',[
                        'class' => 'label',
                        'escape' => false
                    ])?>
                    <?=$this->Form->select('employment_id', [],[
                        'class' => 'form-control',
                        'id' => 'employment-id',
                        'required' => true,
                        'title' => ucwords('please fill out this field'),
                        'empty' => ucwords('Choose Teaching Type')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                    <?=$this->Form->label('position_id', ucwords('Level Of Position Applied For ').'<span class="text-danger">*</span>',[
                        'class' => 'label',
                        'escape' => false
                    ])?>
                    <?=$this->Form->select('position_id', [],[
                        'class' => 'form-control',
                        'id' => 'position-id',
                        'required' => true,
                        'title' => ucwords('please fill out this field'),
                        'empty' => ucwords('Choose Teaching Type')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('barangay_id', ucwords('Barangay Residence ').'<span class="text-danger">*</span>',[
                        'class' => 'label',
                        'escape' => false
                    ])?>
                    <?=$this->Form->select('barangay_id', [],[
                        'class' => 'form-control',
                        'id' => 'barangay-id',
                        'required' => true,
                        'title' => ucwords('please fill out this field'),
                        'empty' => false,
                    ])?>
                    <small id="small-barangay-id"></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('address', ucwords('address ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->text('address',[
                            'class' => 'form-control',
                            'id' => 'address',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter address'),
                            'pattern' => '(.){1,}',
                        ])?>
                        <small></small>
                    </div>
                </div>

            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->hidden('token',[
                'id' => 'token',
                'required' => true,
                'value' => uniqid()
            ])?>
            <?=$this->Form->hidden('is_non_teaching',[
                'id' => 'is-non-teaching',
                'required' => true,
                'value' => intval(10)
            ])?>
            <?= $this->Form->button(__('Submit'),[
                'class' => 'btn btn-success rounded-0',
                'title' => ucwords('Submit')
            ])?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<?=$this->Html->script('admin/teachers/transfer')?>