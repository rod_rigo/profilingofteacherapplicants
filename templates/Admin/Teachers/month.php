<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    var id = parseInt(<?=intval($teachingType->id)?>);
    var teachingType = ('<?=strtoupper($teachingType->teaching_type)?>').toUpperCase();
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Email</th>
                        <th>Birthday</th>
                        <th>Age</th>
                        <th>Civil Status</th>
                        <th>Sex</th>
                        <th>Gender</th>
                        <th>Contact Number</th>
                        <th>Education</th>
                        <th>Eligibility</th>
                        <th>Position</th>
                        <th>Address</th>
                        <th>Religion</th>
                        <th>Ethinicity</th>
                        <th>Answer</th>
                        <th>With Disability</th>
                        <th>Disability</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/teachers/month')?>
