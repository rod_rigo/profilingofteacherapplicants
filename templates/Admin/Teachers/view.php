<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher|\Cake\Collection\CollectionInterface $teacher
 * @var \App\Model\Entity\Employment|\Cake\Collection\CollectionInterface $employment
 */
?>

<script>
    var id = parseInt(<?=intval($teacher->id)?>);
</script>

<div class="row">
    <?php if(!boolval($teacher->is_transfered) && boolval($teacher->is_occupied)):?>
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teachers', 'action' => 'transfer', intval($teacher->id)])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="Transfer" turbolink>
                Transfer
            </a>
        </div>
    <?php endif;?>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($teacher,['type' => 'file', 'id' => 'form', 'class' => 'card card-info']) ?>
            <div class="card-header">
                <h3 class="card-title">Teacher Form</h3>
            </div>

            <div class="card-body">
                <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="mb-4 text-justify text-dark text-capitalize card-info">
                        <?=($employment->description)?>
                    </div>
                </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="form-group">
                            <?=$this->Form->label('employment_code', ucwords('employment code ').'<span class="text-danger">*</span>',[
                                'class' => 'label',
                                'escape' => false
                            ])?>
                            <?=$this->Form->text('employment_code',[
                                'class' => 'form-control',
                                'id' => 'employment-code',
                                'required' => true,
                                'title' => ucwords('Please Fill Out This Field'),
                                'placeholder' => ucwords('Enter Employment Code'),
                                'value' => $teacher->employment_code
                            ])?>
                            <small></small>
                        </div>
                    </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('email', ucwords('email ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->email('email',[
                            'class' => 'form-control',
                            'id' => 'email',
                            'required' => true,
                            'title' => ucwords('Please Enter @ In Email'),
                            'placeholder' => ucwords('Enter Your Email'),
                            'pattern' => '[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*',
                            'value' => $teacher->email
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('first_name', ucwords('first name ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->text('first_name',[
                            'class' => 'form-control',
                            'id' => 'first-name',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter Your First Name'),
                            'pattern' => '(.){1,}',
                            'value' => strtoupper($teacher->first_name)
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('middle_name', ucwords('Middle name ').'<span class="text-info">* (Optional)</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->text('middle_name',[
                            'class' => 'form-control',
                            'id' => 'middle-name',
                            'required' => false,
                            'title' => ucwords('this field is optional'),
                            'placeholder' => ucwords('Enter Your Middle Name (Optional)'),
                            'value' => strtoupper(@$teacher->middle_name)
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('last_name', ucwords('Last name ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->text('last_name',[
                            'class' => 'form-control',
                            'id' => 'last-name',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter Your Last Name'),
                            'pattern' => '(.){1,}',
                            'value' => strtoupper($teacher->last_name)
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('suffix_id', ucwords('Suffix ').'<span class="text-info">* (Optional)</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->select('suffix_id', $suffixes,[
                            'class' => 'form-control',
                            'id' => 'suffix-id',
                            'required' => true,
                            'title' => ucwords('this field is optional'),
                            'empty' => false
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-9 col-lg-9 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('birthday', ucwords('Birthday ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->date('birthday',[
                            'class' => 'form-control',
                            'id' => 'birthday',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('age', ucwords('Age ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->number('age',[
                            'class' => 'form-control',
                            'id' => 'age',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter Your Age'),
                            'pattern' => '(.){1,}',
                            'min' => 18,
                            'value' => intval($teacher->age)
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('civil_status_id', ucwords('civil status ').'<span class="text-danger">*</span>',[
                        'class' => 'label h6',
                        'escape' => false
                    ])?>
                    <div class="form-group d-flex flex-column justify-content-center align-items-start">
                        <?php foreach ($civilStatuses as $key => $value):?>
                            <div class="icheck-primary d-inline my-2">
                                <?=$this->Form->checkbox('civil_status.'.($key),[
                                    'id' => 'civil-status-'.($key),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval(intval($key) == intval($teacher->civil_status_id)),
                                    'value' => intval($key),
                                    'required' => boolval(intval($key) == intval($teacher->civil_status_id)),
                                    'class' => 'civil-status'
                                ])?>
                                <?=$this->Form->label('civil_status.'.($key), ucwords($value),[
                                    'class' => 'text-dark'
                                ])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?=$this->Form->hidden('civil_status_id',[
                        'id' => 'civil-status-id',
                        'required' => true,
                        'value' => intval($teacher->civil_status_id)
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('sex_id', ucwords('Sex ').'<span class="text-danger">*</span>',[
                        'class' => 'label h6',
                        'escape' => false
                    ])?>
                    <div class="form-group d-flex flex-column justify-content-center align-items-start">
                        <?php foreach ($sexes as $key => $value):?>
                            <div class="icheck-primary d-inline my-2">
                                <?=$this->Form->checkbox('sex.'.($key),[
                                    'id' => 'sex-'.($key),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval(intval($key) == intval($teacher->sex_id)),
                                    'value' => intval($key),
                                    'required' => boolval(intval($key) == intval($teacher->sex_id)),
                                    'class' => 'sex'
                                ])?>
                                <?=$this->Form->label('sex.'.($key), ucwords($value),[
                                    'class' => 'text-dark'
                                ])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?=$this->Form->hidden('sex_id',[
                        'id' => 'sex-id',
                        'required' => true,
                        'value' => intval($teacher->sex_id)
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('gender_id', ucwords('Gender ').'<span class="text-danger">*</span>',[
                        'class' => 'label h6',
                        'escape' => false
                    ])?>
                    <div class="form-group d-flex flex-column justify-content-center align-items-start">
                        <?php foreach ($genders as $key => $value):?>
                            <div class="icheck-primary d-inline my-2">
                                <?=$this->Form->checkbox('gender.'.($key),[
                                    'id' => 'gender-'.($key),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval(intval($key) == intval($teacher->gender_id)),
                                    'value' => intval($key),
                                    'required' => boolval(intval($key) == intval($teacher->gender_id)),
                                    'class' => 'gender'
                                ])?>
                                <?=$this->Form->label('gender.'.($key), ucwords($value),[
                                    'class' => 'text-dark'
                                ])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?=$this->Form->hidden('gender_id',[
                        'id' => 'gender-id',
                        'required' => true,
                        'value' => intval($teacher->gender_id)
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('contact_number', ucwords('Contact Number ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->text('contact_number',[
                            'class' => 'form-control',
                            'id' => 'contact-number',
                            'required' => true,
                            'title' => ucwords('Contact number must be start with  0 & 9 followed by 9 digits'),
                            'placeholder' => ucwords('Enter Your Contact Number'),
                            'pattern' => '(09)([0-9]{9})',
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('education_id', ucwords('Education ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->select('education_id', $educations,[
                            'class' => 'form-control',
                            'id' => 'education-id',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Select Education'),
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('eligibility_id', ucwords('Eligibility ').'<span class="text-danger">*</span>',[
                        'class' => 'label h6',
                        'escape' => false
                    ])?>
                    <div class="form-group d-flex flex-column justify-content-center align-items-start">
                        <?php foreach ($eligibilities as $key => $value):?>
                            <div class="icheck-primary d-inline my-2">
                                <?=$this->Form->checkbox('eligibility.'.($key),[
                                    'id' => 'eligibility-'.($key),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval(intval($key) == intval($teacher->eligibility_id)),
                                    'value' => intval($key),
                                    'required' => boolval(intval($key) == intval($teacher->eligibility_id)),
                                    'class' => 'eligibility'
                                ])?>
                                <?=$this->Form->label('eligibility.'.($key), ucwords($value),[
                                    'class' => 'text-dark'
                                ])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?=$this->Form->hidden('eligibility_id',[
                        'id' => 'eligibility-id',
                        'required' => true,
                        'value' => intval($teacher->eligibility_id)
                    ])?>
                    <small></small>
                </div>

                <?php if(!boolval($teacher->is_transfered) && boolval($teacher->is_occupied)):?>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="form-group">
                            <?=$this->Form->label('position_id', ucwords('Level Of Position Applied For '.(strtoupper($teacher->teaching_type->teaching_type)).' Only (Use Transfer To Apply Position In Different Activity)').'<span class="text-danger">*</span>',[
                                'class' => 'label',
                                'escape' => false
                            ])?>
                            <?=$this->Form->select('position_id', $positions,[
                                'class' => 'form-control',
                                'id' => 'position-id',
                                'required' => true,
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Select Position')
                            ])?>
                            <small></small>
                        </div>
                    </div>
                <?php elseif (boolval($teacher->is_transfered) && boolval($teacher->is_occupied)):?>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="form-group">
                            <?=$this->Form->label('position_id', ucwords('Position Already Transfered').'<span class="text-danger">*</span>',[
                                'class' => 'label',
                                'escape' => false
                            ])?>
                            <?=$this->Form->hidden('position_id',[
                                'id' => 'position-id',
                                'required' => true,
                                'value' => intval($teacher->position_id)
                            ])?>
                            <small></small>
                        </div>
                    </div>
                <?php endif;?>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('barangay_id', ucwords('Barangay Residence ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->select('barangay_id', $barangays,[
                            'class' => 'form-control',
                            'id' => 'barangay-id',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'empty' => false,
                            'readonly' => boolval($teacher->is_non_teaching)
                        ])?>
                    </div>
                    <small id="small-barangay-id"></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('address', ucwords('address ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->text('address',[
                            'class' => 'form-control',
                            'id' => 'address',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter address'),
                            'pattern' => '(.){1,}',
                            'readonly' => !boolval($teacher->is_non_teaching)
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('religion_id', ucwords('Religion ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->select('religion_id', $religions,[
                            'class' => 'form-control',
                            'id' => 'religion-id',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Select Religion')
                        ])?>
                    </div>
                    <small id="small-religion-id"></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="form-group">
                        <?=$this->Form->label('ethinicity', ucwords('ethinicity ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->textarea('ethinicity',[
                            'class' => 'form-control',
                            'id' => 'ethinicity',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter Your Answer'),
                            'pattern' => '(.){1,}',
                            'style' => 'resize:none;'
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('has_disability', ucwords('With Disability ').'<span class="text-danger">*</span>',[
                        'class' => 'label h6',
                        'escape' => false
                    ])?>
                    <div class="form-group d-flex flex-column justify-content-center align-items-start">
                        <?php $hasDisabilities = [1 => ucwords('Yes'), 0 => ucwords('No')];?>
                        <?php foreach ($hasDisabilities as $key => $value):?>
                            <div class="icheck-primary d-inline my-2">
                                <?=$this->Form->checkbox('has_disability.'.($key),[
                                    'id' => 'has-disability-'.($key),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval(intval($key) == intval($teacher->has_disability)),
                                    'value' => intval($key),
                                    'required' => boolval(intval($key) == intval($teacher->has_disability)),
                                    'class' => 'has-disability'
                                ])?>
                                <?=$this->Form->label('has_disability.'.($key), ucwords($value),[
                                    'class' => 'text-dark'
                                ])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?=$this->Form->hidden('has_disability',[
                        'id' => 'has-disability',
                        'required' => true,
                        'value' => intval($teacher->has_disability)
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <?=$this->Form->label('disability_id', ucwords('Type Of Disability ').'<span class="text-danger">*</span>',[
                        'class' => 'label h6',
                        'escape' => false
                    ])?>
                    <div class="form-group d-flex flex-column justify-content-center align-items-start">
                        <?php foreach ($disabilities as $key => $disability):?>
                            <div class="icheck-primary d-inline my-2">
                                <?=$this->Form->checkbox('disability.'.($key),[
                                    'id' => 'disability-'.($key),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval(intval($disability->id) == intval($teacher->disability_id)),
                                    'value' => intval($disability->id),
                                    'required' => boolval(intval($disability->id) == intval($teacher->disability_id)),
                                    'class' => 'disability',
                                    'data-target' => intval($disability->is_no)
                                ])?>
                                <?=$this->Form->label('disability.'.($key), ucwords($disability->disability),[
                                    'class' => 'text-dark'
                                ])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?=$this->Form->hidden('disability_id',[
                        'id' => 'disability-id',
                        'required' => true,
                        'value' => intval($teacher->disability_id)
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div id="form-message-success" class="mb-1 text-justify text-dark text-capitalize">
                        <strong>
                            How did you hear about the position?
                        </strong>
                    </div>
                    <div class="form-group">
                        <?=$this->Form->label('answer', ucwords('answer ').'<span class="text-danger">*</span>',[
                            'class' => 'label',
                            'escape' => false
                        ])?>
                        <?=$this->Form->textarea('answer',[
                            'class' => 'form-control',
                            'id' => 'answer',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Enter Your Answer'),
                            'pattern' => '(.){1,}',
                            'style' => 'resize:none;',
                        ])?>
                        <small></small>
                    </div>
                </div>

            </div>
           </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('token',[
                    'id' => 'token',
                    'required' => true,
                    'value' => uniqid()
                ])?>
                <?=$this->Form->hidden('no',[
                    'id' => 'no',
                    'required' => true,
                    'value' => intval($teacher->no)
                ])?>
                <?=$this->Form->hidden('code',[
                    'id' => 'code',
                    'required' => true,
                    'value' => strval($teacher->code)
                ])?>
                <?php
                    /****
                    *=$this->Form->hidden('employment_code',[
                    'id' => 'employment-code',
                    'required' => true,
                    'value' => strval($teacher->employment_code)
                    ])
                     */
                ?>
                <?=$this->Form->hidden('employment_id',[
                    'id' => 'employment-id',
                    'required' => true,
                    'value' => intval($employment->id)
                ])?>
                <?=$this->Form->hidden('teaching_type_id',[
                    'id' => 'teaching-type-id',
                    'required' => true,
                    'value' => intval($teacher->teaching_type_id)
                ])?>
                <?=$this->Form->hidden('is_transfered',[
                    'id' => 'is-transfered',
                    'required' => true,
                    'value' => intval($teacher->is_transfered)
                ])?>
                <?=$this->Form->hidden('is_occupied',[
                    'id' => 'is-occupied',
                    'required' => true,
                    'value' => intval($teacher->is_occupied)
                ])?>
                <?=$this->Form->hidden('is_non_teaching',[
                    'id' => 'is-non-teaching',
                    'required' => true,
                    'value' => intval($teacher->is_non_teaching)
                ])?>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<?=$this->Html->script('admin/teachers/view')?>
