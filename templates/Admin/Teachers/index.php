<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    var id = parseInt(<?=intval($teachingType->id)?>);
    var teachingType = ('<?=strtoupper($teachingType->teaching_type)?>').toUpperCase();
</script>

<?=$this->Form->create(null,['id' => 'form', 'type' => 'file', 'class' => 'row']);?>
<div class="col-sm-12 col-md-3 col-lg-3 mt-3">
    <?=$this->Form->label('start_date', ucwords('start date'));?>
    <?=$this->Form->date('start_date',[
        'class' => 'form-control rounded-0',
        'id' => 'start-date',
        'required' => true,
        'title' => ucwords('pleas fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
    ])?>
</div>
<div class="col-sm-12 col-md-3 col-lg-3 mt-3">
    <?=$this->Form->label('end_date', ucwords('end date'));?>
    <?=$this->Form->date('end_date',[
        'class' => 'form-control rounded-0',
        'id' => 'end-date',
        'required' => true,
        'title' => ucwords('pleas fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
    ])?>
</div>
<div class="col-sm-12 col-md-3 col-lg-3 mt-3">
    <?=$this->Form->label('records', ucwords('records'));?>
    <?=$this->Form->number('records',[
        'class' => 'form-control rounded-0',
        'id' => 'records',
        'required' => true,
        'title' => ucwords('pleas fill out this field'),
        'value' => intval(10000),
        'min' => intval(10000),
        'max' => intval(50000)
    ])?>
</div>
<div class="col-sm-12 col-md-3 col-lg-3 mt-3 d-flex justify-content-start align-items-end">
    <?=$this->Form->button(ucwords('search'),[
        'class' => 'btn btn-primary rounded-0',
        'type' => 'submit'
    ])?>
</div>
<?=$this->Form->end();?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Email</th>
                        <th>Birthday</th>
                        <th>Age</th>
                        <th>Civil Status</th>
                        <th>Sex</th>
                        <th>Gender</th>
                        <th>Contact Number</th>
                        <th>Education</th>
                        <th>Eligibility</th>
                        <th>Position</th>
                        <th>Address</th>
                        <th>Religion</th>
                        <th>Ethinicity</th>
                        <th>Answer</th>
                        <th>With Disability</th>
                        <th>Disability</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/teachers/index')?>
