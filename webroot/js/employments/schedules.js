'use strict';
$(document).ready(function () {

    var baseurl = mainurl+'employment/';

    var calendar = $('#calendar').evoCalendar({
        theme:'Orange Coral',
        language:'en',
        todayHighlight:true,
        sidebarDisplayDefault:false,
        sidebarToggler:true,
        eventDisplayDefault:true,
        eventListToggler:true
    });
    calendar.on('destroy', function(event, evoCalendar) {
        calendar = $('#calendar').evoCalendar({
            theme:'Orange Coral',
            language:'en',
            todayHighlight:true,
            sidebarDisplayDefault:false,
            sidebarToggler:true,
            eventDisplayDefault:true,
            eventListToggler:true
        });
    });

    function getEmployments() {
        var array = [];
        $.ajax({
            url:baseurl+'getEmployments',
            type: 'GET',
            method: 'GET',
            async:false,
            global:false,
            dataType:'JSON',
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                var description = (data.description).replace(/<\/?[^>]+>/gi, '');
                array.push({
                    id: data.id,
                    name:(moment(data.start_date).format('Y-MM-DD'))+' - '+(moment(data.end_date).format('Y-MM-DD')),
                    badge: 'Start',
                    description: '<h6 class="text-justify">'+(data.employment)+'</h6><br><div title="'+(description)+'" style="text-align: justify;">'+((description).substr(0, 150))+'...</div>',
                    date: data.start_date,
                    type: 'Events',
                    color: '#fece09',
                    everyYear: parseInt(data.is_per_year),
                });
                array.push({
                    id: data.id,
                    name:(moment(data.start_date).format('Y-MM-DD'))+' - '+(moment(data.end_date).format('Y-MM-DD')),
                    badge: 'End',
                    description: '<h6 class="text-justify">'+(data.employment)+'</h6><br><div title="'+(description)+'" style="text-align: justify;">'+((description).substr(0, 150))+'...</div>',
                    date: data.end_date,
                    type: 'Events',
                    color: '#EC3125',
                    everyYear: parseInt(data.is_per_year),
                });
            });
        }).fail(function (xhr, status, error) {
            const response = JSON.parse(xhr.responseText);
            swal(response.icon, response.result, response.message);
        });
        return array;
    }

    calendar.evoCalendar('addCalendarEvent', getEmployments());

    function swal(icon, result, message) {
        Swal.fire({
            icon:icon,
            title:result,
            text:message,
            timer:5000
        });
    }

});