'use strict';
$(document).ready(function (e) {

    if(!is_non_teaching){

        $(function (e) {

            var baseurl = mainurl+'dashboards/';

            const autocolors = window['chartjs-plugin-autocolors'];
            Chart.register(autocolors);

            $('#form').submit(function (e) {
                e.preventDefault();
                $('#start-date, #end-date, button[type="submit"]').prop('disabled', true);
                teacher_chart.destroy();
                getTeacherChart();
                gender_chart.destroy();
                getTeacherGendersChart();
                civil_status_chart.destroy();
                getTeacherCivilStatusChart();
                religion_chart.destroy();
                getTeacherReligionsChart();
                education_chart.destroy();
                getTeacherEducationsChart();
                barangay_chart.destroy();
                getTeacherBarangaysChart();
                $('#start-date, #end-date, button[type="submit"]').prop('disabled', false);
            });

            $('#xlsx').click(function (e) {
                var url = baseurl+'xlsx/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD'));
                Swal.fire({
                    title: 'Export All?',
                    text: 'Are You Sure',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        window.open(url);
                    }
                });
            });

            function count() {
                $.ajax({
                    url: baseurl+'count/'+(parseInt(id)),
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        $('#total-teachers-today, #total-teachers-week, #total-teachers-month, #total-teachers-year').text(parseInt(0));
                    },
                }).done(function (data, status, xhr) {
                    $('#total-teachers-today').text(parseInt(data.today));
                    $('#total-teachers-week').text(parseInt(data.week));
                    $('#total-teachers-month').text(parseInt(data.month));
                    $('#total-teachers-year').text(parseInt(data.year));
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
            }

            var teacher_canvas = document.querySelector('#teacher-chart').getContext('2d');
            var teacher_chart;

            function getTeacher() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacher/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.month).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherChart() {
                teacher_chart =  new Chart(teacher_canvas, {
                    type: 'bar',
                    data: {
                        labels: getTeacher().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacher().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 1
                            },
                            title: {
                                display: true,
                                text: 'Applicants Per Months',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var gender_canvas = document.querySelector('#gender-chart').getContext('2d');
            var gender_chart;

            function getTeacherGenders() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherGenders/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.gender).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherGendersChart() {
                gender_chart =  new Chart(gender_canvas, {
                    type: 'pie',
                    data: {
                        labels: getTeacherGenders().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherGenders().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 2
                            },
                            title: {
                                display: true,
                                text: 'Gender',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var civil_status_canvas = document.querySelector('#civil-status-chart').getContext('2d');
            var civil_status_chart;

            function getTeacherCivilStatus() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherCivilStatus/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.civil_status).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherCivilStatusChart() {
                civil_status_chart =  new Chart(civil_status_canvas, {
                    type: 'doughnut',
                    data: {
                        labels: getTeacherCivilStatus().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherCivilStatus().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 3
                            },
                            title: {
                                display: true,
                                text: 'Civil Status',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var religion_canvas = document.querySelector('#religion-chart').getContext('2d');
            var religion_chart;

            function getTeacherReligions() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherReligions/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.religion).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherReligionsChart() {
                religion_chart =  new Chart(religion_canvas, {
                    type: 'bar',
                    data: {
                        labels: getTeacherReligions().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherReligions().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'y',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 4
                            },
                            title: {
                                display: true,
                                text: 'Religion',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var education_canvas = document.querySelector('#education-chart').getContext('2d');
            var education_chart;

            function getTeacherEducations() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherEducations/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.education).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherEducationsChart() {
                education_chart =  new Chart(education_canvas, {
                    type: 'line',
                    data: {
                        labels: getTeacherEducations().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherEducations().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 5
                            },
                            title: {
                                display: true,
                                text: 'Education',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var barangay_canvas = document.querySelector('#barangay-chart').getContext('2d');
            var barangay_chart;

            function getTeacherBarangays() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherBarangays/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.barangay).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherBarangaysChart() {
                barangay_chart =  new Chart(barangay_canvas, {
                    type: 'polarArea',
                    data: {
                        labels: getTeacherBarangays().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherBarangays().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 6
                            },
                            title: {
                                display: true,
                                text: 'Barangay',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            count();
            getTeacherChart();
            getTeacherGendersChart();
            getTeacherCivilStatusChart();
            getTeacherReligionsChart();
            getTeacherEducationsChart();
            getTeacherBarangaysChart();

            setInterval(function (e) {
                count();
                teacher_chart.destroy();
                getTeacherChart();
                gender_chart.destroy();
                getTeacherGendersChart();
                civil_status_chart.destroy();
                getTeacherCivilStatusChart();
                religion_chart.destroy();
                getTeacherReligionsChart();
                education_chart.destroy();
                getTeacherEducationsChart();
                barangay_chart.destroy();
                getTeacherBarangaysChart();
            }, (parseInt(15) * parseInt(60) * parseInt(1000)));

        });

    }else{

        $(function (e) {

            var baseurl = mainurl+'dashboards/';

            const autocolors = window['chartjs-plugin-autocolors'];
            Chart.register(autocolors);

            $('#form').submit(function (e) {
                e.preventDefault();
                $('#start-date, #end-date, button[type="submit"]').prop('disabled', true);
                teacher_chart.destroy();
                getTeacherChart();
                gender_chart.destroy();
                getTeacherGendersChart();
                civil_status_chart.destroy();
                getTeacherCivilStatusChart();
                religion_chart.destroy();
                getTeacherReligionsChart();
                education_chart.destroy();
                getTeacherEducationsChart();
                $('#start-date, #end-date, button[type="submit"]').prop('disabled', false);
            });

            $('#xlsx').click(function (e) {
                var url = baseurl+'xlsx/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD'));
                Swal.fire({
                    title: 'Export All?',
                    text: 'Are You Sure',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        window.open(url);
                    }
                });
            });

            function count() {
                $.ajax({
                    url: baseurl+'count/'+(parseInt(id)),
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        $('#total-teachers-today, #total-teachers-week, #total-teachers-month, #total-teachers-year').text(parseInt(0));
                    },
                }).done(function (data, status, xhr) {
                    $('#total-teachers-today').text(parseInt(data.today));
                    $('#total-teachers-week').text(parseInt(data.week));
                    $('#total-teachers-month').text(parseInt(data.month));
                    $('#total-teachers-year').text(parseInt(data.year));
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
            }

            var teacher_canvas = document.querySelector('#teacher-chart').getContext('2d');
            var teacher_chart;

            function getTeacher() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacher/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.month).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherChart() {
                teacher_chart =  new Chart(teacher_canvas, {
                    type: 'bar',
                    data: {
                        labels: getTeacher().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacher().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 1
                            },
                            title: {
                                display: true,
                                text: 'Applicants Per Months',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var gender_canvas = document.querySelector('#gender-chart').getContext('2d');
            var gender_chart;

            function getTeacherGenders() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherGenders/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.gender).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherGendersChart() {
                gender_chart =  new Chart(gender_canvas, {
                    type: 'pie',
                    data: {
                        labels: getTeacherGenders().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherGenders().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 2
                            },
                            title: {
                                display: true,
                                text: 'Gender',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var civil_status_canvas = document.querySelector('#civil-status-chart').getContext('2d');
            var civil_status_chart;

            function getTeacherCivilStatus() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherCivilStatus/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.civil_status).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherCivilStatusChart() {
                civil_status_chart =  new Chart(civil_status_canvas, {
                    type: 'doughnut',
                    data: {
                        labels: getTeacherCivilStatus().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherCivilStatus().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 3
                            },
                            title: {
                                display: true,
                                text: 'Civil Status',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var religion_canvas = document.querySelector('#religion-chart').getContext('2d');
            var religion_chart;

            function getTeacherReligions() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherReligions/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.religion).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherReligionsChart() {
                religion_chart =  new Chart(religion_canvas, {
                    type: 'bar',
                    data: {
                        labels: getTeacherReligions().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherReligions().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'y',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 4
                            },
                            title: {
                                display: true,
                                text: 'Religion',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            var education_canvas = document.querySelector('#education-chart').getContext('2d');
            var education_chart;

            function getTeacherEducations() {
                var array = {
                    'data':[],
                    'label':[]
                };
                $.ajax({
                    url:baseurl+'getTeacherEducations/'+(parseInt(id))+'?employment_id='+($('#employment-id').val())+'&start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
                    method: 'GET',
                    type:'GET',
                    dataType:'JSON',
                    async: false,
                    global: false,
                    beforeSend: function (e) {

                    },
                }).done(function (data, status, xhr) {
                    $.map(data,function (data) {
                        array.data.push(parseFloat(data.total).toFixed(2));
                        array.label.push((data.education).toUpperCase());
                    });
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
                return array;
            }

            function getTeacherEducationsChart() {
                education_chart =  new Chart(education_canvas, {
                    type: 'line',
                    data: {
                        labels: getTeacherEducations().label,
                        datasets: [
                            {
                                label:'Total',
                                data: getTeacherEducations().data,
                                pointRadius: 5,
                                pointHoverRadius: 7,
                            }
                        ]
                    },
                    options: {
                        indexAxis:'x',
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            autocolors: {
                                mode: 'data',
                                offset: 5
                            },
                            title: {
                                display: true,
                                text: 'Education',
                                font: {
                                    size: 14
                                }
                            },
                            subtitle: {
                                display: false,
                                text: ''
                            },
                            legend: {
                                display:true,
                                onClick: function (evt, legendItem, legend) {
                                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                                    legend.chart.toggleDataVisibility(index);
                                    legend.chart.update();
                                },
                                labels:{
                                    generateLabels: function (chart) {
                                        return  $.map(chart.data.labels, function( label, index ) {
                                            return {
                                                text:label,
                                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                                hidden: !chart.getDataVisibility(index)
                                            };
                                        });
                                    },
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                        }
                    },
                    plugins:[
                        {
                            id: null,
                            beforeDraw: function (chart) {
                                const ctx = chart.canvas.getContext('2d');
                                ctx.save();
                                ctx.globalCompositeOperation = 'destination-over';
                                ctx.fillStyle = 'white';
                                ctx.fillRect(0, 0, chart.width, chart.height);
                                ctx.restore();
                            },
                        }
                    ]
                });
            }

            count();
            getTeacherChart();
            getTeacherGendersChart();
            getTeacherCivilStatusChart();
            getTeacherReligionsChart();
            getTeacherEducationsChart();

            setInterval(function (e) {
                count();
                teacher_chart.destroy();
                getTeacherChart();
                gender_chart.destroy();
                getTeacherGendersChart();
                civil_status_chart.destroy();
                getTeacherCivilStatusChart();
                religion_chart.destroy();
                getTeacherReligionsChart();
                education_chart.destroy();
                getTeacherEducationsChart();
            }, (parseInt(15) * parseInt(60) * parseInt(1000)));

        });
    }

});
$(document).ready(function (e) {

    var filename = function (string) {
        var startDate = $('#start-date').val();
        var endDate = $('#end-date').val();
        return (string)+ ' ' + (startDate)+ ' - '+ endDate;
    };

    $('.png').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        var chart = document.querySelector('#'+(dataTarget)+'');
        var title = $(this).attr('data-title');
        Swal.fire({
            title: 'Export Chart?',
            text: 'Are You Sure',
            iconHtml: '<i class="fas fa-file-image text-info"></i>',
            iconColor:'#48abf7',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                download(chart.toDataURL('image/jpeg', 1.0), (filename(title))+'.jpeg', 'image/jpeg');
            }
        });
    });

    $('.print').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        var chart =  document.querySelector('#'+(dataTarget)+'');
        var title = $(this).attr('data-title');
        Swal.fire({
            title: 'Export Chart?',
            text: 'Are You Sure',
            iconHtml: '<i class="fas fa-print text-secondary"></i>',
            iconColor:'#6861ce',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                printJS({
                    printable: chart.toDataURL('image/png', 1.0),
                    type: 'image',
                    imageStyle: 'width:100%;height:90vh;margin-bottom:20px;',
                    style: '@page { size: Letter landscape; }',
                });
            }
        });
    });

    $('.excel').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        var canvas =  document.querySelector('#'+(dataTarget)+'');
        var chart = Chart.getChart(canvas);
        var title = $(this).attr('data-title');
        Swal.fire({
            title: 'Export Chart?',
            text: 'Are You Sure',
            iconHtml: '<i class="fas fa-file-excel text-success"></i>',
            iconColor:'#31ce36',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {

                var workbook = new ExcelJS.Workbook();

                workbook.creator = auth.username;
                workbook.lastModifiedBy = auth.username;
                workbook.created = new Date();
                workbook.modified = new Date();
                workbook.lastPrinted = new Date();

                var worksheet = workbook.addWorksheet('New Sheet');
                worksheet.columns = [
                    { header: 'Label', key: 'value' },
                    { header: 'Total', key: 'data' }
                ];

                var image = canvas.toDataURL('image/jpeg', 1.0);
                image = workbook.addImage({
                    base64: image,
                    extension: 'jpeg'
                });
                worksheet.addImage(image, {
                    tl: { col: 5, row: 0 },
                    ext: { width: canvas.width, height: canvas.height }
                });

                var rows = [];

                for(var i = 0; i < chart.config.data.labels.length; i++){
                    rows.push({
                        value : chart.config.data.labels[i],
                        data : chart.config.data.datasets[0].data[i],
                    });
                }

                worksheet.addRows(rows);

                workbook.xlsx.writeBuffer().then(function (data) {
                    var blob = new Blob([data], {
                        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
                    });
                    saveAs(blob, (filename(title))+'.xlsx');
                });

            }
        });
    });

});