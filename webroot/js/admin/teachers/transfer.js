$(document).ready(function (e) {

    const baseurl = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            Turbolinks.visit(data.redirect,{action: 'advance'});
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#teaching-type-id').change(function (e) {

        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        getEmploymentLists(value);
        getPositionLists(value);
        getBarangaysList(value);
        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#employment-id, #position-id').change(function (e) {

        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#barangay-id').select2({
        placeholder: 'Choose Teaching Type',
        allowClear: true,
        width: '100%',
    }).on('select2:close', function (e) {
        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid');
            $('#small-barangay-id').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid');
        $('#small-barangay-id').empty();
    });

    function getEmploymentLists(teachingTypeId) {
        $.ajax({
            url: mainurl + 'employments/getEmploymentLists/'+(teachingTypeId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('#employment-id').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#employment-id').empty().append('<option value="">Select Activity</option>');
            $.map(data, function (data, key) {
                $('#employment-id').append('<option value="'+(parseInt(key))+'">'+(data)+'</option>');
            });
            $('#employment-id').prop('disabled', false);
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('warning', null, data.responseJSON.message);
        });
    }

    function getPositionLists(teachingTypeId) {
        $.ajax({
            url: mainurl + 'positions/getPositionLists/'+(teachingTypeId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('#position-id').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#position-id').empty().append('<option value="">Select Position</option>');
            $.map(data, function (data, key) {
                $('#position-id').append('<option value="'+(parseInt(key))+'">'+(data)+'</option>');
            });
            $('#position-id').prop('disabled', false);
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('warning', null, data.responseJSON.message);
        });
    }

    function getBarangaysList(teachingTypeId) {
        $.ajax({
            url: mainurl + 'barangays/getBarangaysList/'+(teachingTypeId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('#barangay-id').select2('destroy');
                $('#barangay-id').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#is_non_teaching').val(parseInt(data.teaching_type.is_non_teaching));
            if(data.teaching_type.is_non_teaching){
                $('#address').val(address);
                $('#barangay-id').empty();
                $.map(data.barangays, function (data, key) {
                    $('#barangay-id').append('<option value="'+(parseInt(data.id))+'">'+(data.barangay)+'</option>');
                });
                $('#barangay-id').prop('disabled', false);
            }else{
                $('#barangay-id').empty().append('<option value="">Select Barangay</option>');
                $.map(data.barangays, function (data, key) {
                    $('#barangay-id').append('<option value="'+(parseInt(data.id))+'">'+(data.barangay)+'</option>');
                });
                $('#barangay-id').prop('disabled', false);
                $('#address').val('-');
            }
            $('#barangay-id').select2({
                placeholder: 'Select Barangay',
                allowClear: true,
                width: '100%',
            }).on('select2:close', function (e) {
                var value = $(this).val();

                if(!value){
                    $(this).addClass('is-invalid');
                    $('#small-barangay-id').text('Please Fill Out This Field');
                    return true;
                }

                $(this).removeClass('is-invalid');
                $('#small-barangay-id').empty();
            });
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('warning', null, data.responseJSON.message);
        });
    }

});