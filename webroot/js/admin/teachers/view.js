'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'teachers/edit/'+(id);

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action:'replace'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

        });
    });

    $('#email').on('input', function (e) {
        var value = $(this).val();
        var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Enter @ In Email');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();
    });

    $('#answer, #first-name, #last-name, #ethinicity, #address').on('input', function (e) {
        var value = $(this).val();
        var regex = /^(.){1,}$/;

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();
    });

    $('#birthday').change(function (e) {
        var value = (new Date($(this).val())).getYear();
        var year = (new Date()).getYear();
        var age = parseInt(year) - parseInt(value);
        $('#age').val(age);
    });

    $('.civil-status').focus(function (e) {
        if(!$('#civil-status-id').val()){
            $('#civil-status-id').next('small').text('Select Civil Status');
        }
    }).change(function (e) {
        var value = $(this).val();
        $('.civil-status').removeAttr('required').prop('checked', false).not(this);
        $(this).prop('checked', true);
        $('#civil-status-id').val(value).next('small').empty();
    });

    $('.sex').focus(function (e) {
        if(!$('#sex-id').val()){
            $('#sex-id').next('small').text('Select Sex');
        }
    }).change(function (e) {
        var value = $(this).val();
        $('.sex').removeAttr('required').prop('checked', false).not(this);
        $(this).prop('checked', true);
        $('#sex-id').val(value).next('small').empty();
    });

    $('.gender').focus(function (e) {
        if(!$('#gender-id').val()){
            $('#gender-id').next('small').text('Select Gender');
        }
    }).change(function (e) {
        var value = $(this).val();
        $('.gender').removeAttr('required').prop('checked', false).not(this);
        $(this).prop('checked', true);
        $('#gender-id').val(value).next('small').empty();
    });

    $('#contact-number').keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }

    }).on('input', function (e) {
        var value = $(this).val();
        var regex = /^(09)([0-9]{9})$/;

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Contact Number Must Be Start With  0 & 9 Followed By 9 Digits');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#education-id').change(function (e) {

        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('.eligibility').focus(function (e) {
        if(!$('#eligibility-id').val()){
            $('#eligibility-id').next('small').text('Select Eligibility');
        }
    }).change(function (e) {
        var value = $(this).val();
        $('.eligibility').removeAttr('required').prop('checked', false).not(this);
        $(this).prop('checked', true);
        $('#eligibility-id').val(value).next('small').empty();
    });

    $('#position-id').change(function (e) {

        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#barangay-id').select2({
        placeholder: 'Select Barangay Residence',
        allowClear: true,
        width: '100%'
    }).on('select2:close', function (e) {
        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid');
            $('#small-barangay-id').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid');
        $('#small-barangay-id').empty();
    });

    $('#religion-id').select2({
        placeholder: 'Select Religion',
        allowClear: true,
        width: '100%'
    }).on('select2:close', function (e) {
        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid');
            $('#small-religion-id').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid');
        $('#small-religion-id').empty();
    });

    $('.has-disability').focus(function (e) {
        if(!$('#has-disability').val()){
            $('#has-disability').next('small').text('Select With Disability');
        }
    }).change(function (e) {
        var value = $(this).val();
        $('.has-disability').removeAttr('required').prop('checked', false).not(this);
        $(this).prop('checked', true);
        $('#has-disability').val(value).next('small').empty();
        if(parseInt(value) == parseInt(0)){
            $('.disability').removeAttr('required').prop('checked', false);
            $('.disability[data-target="1"]').prop('checked', true);
            $('#disability-id').val(parseInt($('.disability[data-target="1"]').val()));
        }else{
            $('.disability').prop('required', true);
            $('.disability[data-target="1"]').prop('checked', false);
            $('#disability-id').val('');
        }
    });

    $('.disability').focus(function (e) {
        if(!$('#disability-id').val()){
            $('#disability-id').next('small').text('Select Disability');
        }
    }).change(function (e) {
        var value = $(this).val();
        var dataTarget = $(this).attr('data-target');
        $('.disability').removeAttr('required').prop('checked', false).not(this);
        $(this).prop('checked', true);
        $('#disability-id').val(value).next('small').empty();
        if(parseInt(dataTarget) == parseInt(0)){
            $('.has-disability').prop('checked', false);
            $('.has-disability[value="1"]').prop('checked', true);
            $('#has-disability').val(1).next('small').empty();
        }else{
            $('.has-disability').prop('checked', false);
            $('.has-disability[value="0"]').prop('checked', true);
            $('#has-disability').val(0).next('small').empty();
        }
    });

    $('#suffix-id').change(function (e) {

        var value = $(this).val();

        if(!value){
            $(this).removeClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

});