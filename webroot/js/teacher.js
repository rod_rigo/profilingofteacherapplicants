
Turbolinks.start();
$('a[turbolink]').click(function (e) {
    var href = $(this).attr('href');
    Turbolinks.visit(href,{action:'replace'});
});
$('input[type="number"]').keypress(function (e) {
    var key = e.key;
    var regex = /^([0-9]){1,}$/;
    if(!key.match(regex)){
        e.preventDefault();
    }
});
