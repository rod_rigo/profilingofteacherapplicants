Turbolinks.start();

$('a[turbolink]').click(function (e) {
    var href = $(this).attr('href');
    Turbolinks.visit(href,{action:'replace'});
});

$('input[type="number"]').keypress(function (e) {
    var key = e.key;
    var regex = /^([0-9]){1,}$/;
    if(!key.match(regex)){
        e.preventDefault();
    }
});

function swal(icon, result, message){
    Swal.fire({
        icon: icon,
        title: message,
        text: null,
        timer: 1000,
        timerProgressBar:true,
        toast:true,
        position:'top-right',
        showConfirmButton:false,
    });
}