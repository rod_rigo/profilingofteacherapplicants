/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : sdo_teacher_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 25/06/2024 16:19:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for teaching_types
-- ----------------------------
DROP TABLE IF EXISTS `teaching_types`;
CREATE TABLE `teaching_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `teaching_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_non_teaching` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teaching_types to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `teaching_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teaching_types
-- ----------------------------
INSERT INTO `teaching_types` VALUES (1, 1, 'NON-TEACHING', 1, 1, '2024-06-25 23:06:12', '2024-06-26 00:16:18', NULL);
INSERT INTO `teaching_types` VALUES (2, 1, 'TEACHING', 0, 1, '2024-06-26 00:16:15', '2024-06-26 00:16:15', NULL);

SET FOREIGN_KEY_CHECKS = 1;
