/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : sdo_teacher_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 25/06/2024 17:08:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barangays
-- ----------------------------
DROP TABLE IF EXISTS `barangays`;
CREATE TABLE `barangays`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `barangay` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `barangays to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `barangays to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of barangays
-- ----------------------------
INSERT INTO `barangays` VALUES (1, 1, 'Abra', 1, '2024-02-27 23:16:46', '2024-02-28 00:40:50', NULL);
INSERT INTO `barangays` VALUES (2, 1, 'Ambalatungan', 1, '2024-02-28 02:31:13', '2024-02-28 02:31:13', NULL);
INSERT INTO `barangays` VALUES (3, 1, 'Balintocatoc', 1, '2024-02-28 02:31:22', '2024-02-28 02:31:22', NULL);
INSERT INTO `barangays` VALUES (4, 1, 'Baluarte', 1, '2024-02-28 02:31:30', '2024-02-28 02:31:30', NULL);
INSERT INTO `barangays` VALUES (5, 1, 'Bannawag Norte', 1, '2024-02-28 02:31:43', '2024-02-28 02:31:43', NULL);
INSERT INTO `barangays` VALUES (6, 1, 'Batal', 1, '2024-02-28 02:31:52', '2024-02-28 02:31:52', NULL);
INSERT INTO `barangays` VALUES (7, 1, 'Buenavista', 1, '2024-02-28 02:32:02', '2024-02-28 02:32:02', NULL);
INSERT INTO `barangays` VALUES (8, 1, 'Cabulay', 1, '2024-02-28 02:32:13', '2024-02-28 02:32:13', NULL);
INSERT INTO `barangays` VALUES (9, 1, 'Calao East (Poblacion)', 1, '2024-02-28 02:32:27', '2024-02-28 02:32:27', NULL);
INSERT INTO `barangays` VALUES (10, 1, 'Calao West (Poblacion)', 1, '2024-02-28 02:32:41', '2024-02-28 02:32:41', NULL);
INSERT INTO `barangays` VALUES (11, 1, 'Calaocan', 1, '2024-02-28 02:32:50', '2024-02-28 02:32:50', NULL);
INSERT INTO `barangays` VALUES (12, 1, 'Centro East (Poblacion)', 1, '2024-02-28 02:33:04', '2024-02-28 02:33:04', NULL);
INSERT INTO `barangays` VALUES (13, 1, 'Centro West (Poblacion)', 1, '2024-02-28 02:33:45', '2024-02-28 02:33:45', NULL);
INSERT INTO `barangays` VALUES (14, 1, 'Divisoria', 1, '2024-02-28 02:33:58', '2024-02-28 02:33:58', NULL);
INSERT INTO `barangays` VALUES (15, 1, 'Dubinan East', 1, '2024-02-28 02:34:08', '2024-02-28 02:34:08', NULL);
INSERT INTO `barangays` VALUES (16, 1, 'Dubinan West', 1, '2024-02-28 02:34:15', '2024-02-28 02:34:15', NULL);
INSERT INTO `barangays` VALUES (17, 1, 'Luna', 1, '2024-02-28 02:34:22', '2024-02-28 02:34:22', NULL);
INSERT INTO `barangays` VALUES (18, 1, 'Mabini', 1, '2024-02-28 02:34:32', '2024-02-28 02:34:32', NULL);
INSERT INTO `barangays` VALUES (19, 1, 'Malvar', 1, '2024-02-28 02:34:40', '2024-02-28 02:34:40', NULL);
INSERT INTO `barangays` VALUES (20, 1, 'Nabbuan', 1, '2024-02-28 02:34:47', '2024-02-28 02:34:47', NULL);
INSERT INTO `barangays` VALUES (21, 1, 'Naggasican', 1, '2024-02-28 02:34:57', '2024-02-28 02:34:57', NULL);
INSERT INTO `barangays` VALUES (22, 1, 'Patul', 1, '2024-02-28 02:35:14', '2024-02-28 02:35:14', NULL);
INSERT INTO `barangays` VALUES (23, 1, 'Plaridel', 1, '2024-02-28 02:35:24', '2024-02-28 02:35:24', NULL);
INSERT INTO `barangays` VALUES (24, 1, 'Rizal', 1, '2024-02-28 02:35:32', '2024-02-28 02:35:32', NULL);
INSERT INTO `barangays` VALUES (25, 1, 'Rosario', 1, '2024-02-28 02:35:40', '2024-02-28 02:35:40', NULL);
INSERT INTO `barangays` VALUES (26, 1, 'Sagana', 1, '2024-02-28 02:35:48', '2024-02-28 02:35:48', NULL);
INSERT INTO `barangays` VALUES (27, 1, 'Salvador', 1, '2024-02-28 02:35:54', '2024-02-28 02:35:54', NULL);
INSERT INTO `barangays` VALUES (28, 1, 'San Andres', 1, '2024-02-28 02:36:12', '2024-02-28 02:36:12', NULL);
INSERT INTO `barangays` VALUES (29, 1, 'San Isidro', 1, '2024-02-28 02:36:23', '2024-02-28 02:36:23', NULL);
INSERT INTO `barangays` VALUES (30, 1, 'San Jose', 1, '2024-02-28 02:36:33', '2024-02-28 02:36:33', NULL);
INSERT INTO `barangays` VALUES (31, 1, 'Sinili', 1, '2024-02-28 02:59:52', '2024-02-28 03:00:01', NULL);
INSERT INTO `barangays` VALUES (32, 1, 'Sinsayon', 1, '2024-02-28 03:00:12', '2024-02-28 03:00:12', NULL);
INSERT INTO `barangays` VALUES (33, 1, 'Sta. Rosa', 1, '2024-02-28 03:00:22', '2024-02-28 03:00:22', NULL);
INSERT INTO `barangays` VALUES (34, 1, 'Victory Norte', 1, '2024-02-28 03:00:34', '2024-02-28 03:00:34', NULL);
INSERT INTO `barangays` VALUES (35, 1, 'Victory Sur', 1, '2024-02-28 03:00:48', '2024-02-28 03:00:48', NULL);
INSERT INTO `barangays` VALUES (36, 1, 'Villasis', 1, '2024-02-28 03:00:56', '2024-02-28 03:00:56', NULL);
INSERT INTO `barangays` VALUES (37, 1, 'Villa Gonzaga', 1, '2024-02-28 03:01:05', '2024-02-28 03:01:05', NULL);

-- ----------------------------
-- Table structure for civil_statuses
-- ----------------------------
DROP TABLE IF EXISTS `civil_statuses`;
CREATE TABLE `civil_statuses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `civil_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `civil_statuses to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `civil_statuses to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of civil_statuses
-- ----------------------------
INSERT INTO `civil_statuses` VALUES (1, 1, 'Single', 1, '2024-02-28 02:24:46', '2024-02-28 02:24:46', NULL);
INSERT INTO `civil_statuses` VALUES (2, 1, 'Married', 1, '2024-02-28 02:24:56', '2024-02-28 02:24:56', NULL);
INSERT INTO `civil_statuses` VALUES (3, 1, 'Widowed', 1, '2024-02-28 02:25:08', '2024-02-28 02:25:08', NULL);
INSERT INTO `civil_statuses` VALUES (4, 1, 'Legally Separated', 1, '2024-02-28 02:25:19', '2024-02-28 02:25:19', NULL);
INSERT INTO `civil_statuses` VALUES (5, 1, 'Annuled', 1, '2024-02-28 02:25:30', '2024-02-28 02:25:30', NULL);

-- ----------------------------
-- Table structure for disabilities
-- ----------------------------
DROP TABLE IF EXISTS `disabilities`;
CREATE TABLE `disabilities`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `disability` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` tinyint NOT NULL DEFAULT 0,
  `is_no` tinyint NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of disabilities
-- ----------------------------
INSERT INTO `disabilities` VALUES (1, 1, 'Acquired Brain Injury', 1, 0, 1, '2024-02-28 03:51:12', '2024-02-28 03:51:12', NULL);
INSERT INTO `disabilities` VALUES (2, 1, 'Asperger Syndrome', 2, 0, 1, '2024-02-28 03:51:26', '2024-02-28 03:51:26', NULL);
INSERT INTO `disabilities` VALUES (3, 1, 'Attention Deficit/Hyperactivity Disorder (ADHD)', 3, 0, 1, '2024-02-28 03:51:54', '2024-02-28 03:51:54', NULL);
INSERT INTO `disabilities` VALUES (4, 1, 'Autism', 4, 0, 1, '2024-02-28 03:52:01', '2024-02-28 03:52:01', NULL);
INSERT INTO `disabilities` VALUES (5, 1, 'Autistic Spectrum Disorder', 5, 0, 1, '2024-02-28 03:52:15', '2024-02-28 03:52:15', NULL);
INSERT INTO `disabilities` VALUES (6, 1, 'Blindness', 6, 0, 1, '2024-02-28 03:52:25', '2024-02-28 03:52:25', NULL);
INSERT INTO `disabilities` VALUES (7, 1, 'Cerebral Palsy', 7, 0, 1, '2024-02-28 03:52:36', '2024-02-28 03:52:36', NULL);
INSERT INTO `disabilities` VALUES (8, 1, 'Deafblindness', 8, 0, 1, '2024-02-28 03:52:46', '2024-02-28 03:52:46', NULL);
INSERT INTO `disabilities` VALUES (9, 1, 'Developmental Coordination Disorder', 9, 0, 1, '2024-02-28 03:53:03', '2024-02-28 03:53:03', NULL);
INSERT INTO `disabilities` VALUES (10, 1, 'Developmental Disability', 10, 0, 1, '2024-02-28 03:53:19', '2024-02-28 03:53:19', NULL);
INSERT INTO `disabilities` VALUES (11, 1, 'Dwarfism', 11, 0, 1, '2024-02-28 03:53:32', '2024-02-28 03:53:32', NULL);
INSERT INTO `disabilities` VALUES (12, 1, 'Dysgraphia', 12, 0, 1, '2024-02-28 03:53:41', '2024-02-28 03:53:41', NULL);
INSERT INTO `disabilities` VALUES (13, 1, 'Dyslexia', 13, 0, 1, '2024-02-28 03:53:49', '2024-02-28 03:53:49', NULL);
INSERT INTO `disabilities` VALUES (14, 1, 'Hearing Impairment', 14, 0, 1, '2024-02-28 03:54:00', '2024-02-28 03:54:00', NULL);
INSERT INTO `disabilities` VALUES (15, 1, 'Invisible Disability', 15, 0, 1, '2024-02-28 03:54:22', '2024-02-28 03:54:22', NULL);
INSERT INTO `disabilities` VALUES (16, 1, 'Intellectual Disability', 16, 0, 1, '2024-02-28 03:54:35', '2024-02-28 03:54:35', NULL);
INSERT INTO `disabilities` VALUES (17, 1, 'Learning Disability', 17, 0, 1, '2024-02-28 03:54:46', '2024-02-28 03:54:46', NULL);
INSERT INTO `disabilities` VALUES (18, 1, 'Spina Bifida', 18, 0, 1, '2024-02-28 03:54:57', '2024-02-28 03:54:57', NULL);
INSERT INTO `disabilities` VALUES (19, 1, 'Traumatic Brain Injury', 19, 0, 1, '2024-02-28 03:55:14', '2024-02-28 03:55:14', NULL);
INSERT INTO `disabilities` VALUES (20, 1, 'Vision Impairment', 20, 0, 1, '2024-02-28 03:55:25', '2024-02-28 03:55:25', NULL);
INSERT INTO `disabilities` VALUES (21, 1, 'None', 21, 1, 1, '2024-02-28 22:58:38', '2024-02-29 01:00:25', NULL);
INSERT INTO `disabilities` VALUES (22, 1, 'Yonex', 22, 0, 1, '2024-02-29 01:01:51', '2024-02-29 01:01:51', '2024-02-28 17:02:03');

-- ----------------------------
-- Table structure for educations
-- ----------------------------
DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `education` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `educations to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `educations to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of educations
-- ----------------------------
INSERT INTO `educations` VALUES (1, 1, 'Bachelor Of Elementary Education', 1, '2024-02-28 02:29:02', '2024-02-28 02:29:02', NULL);
INSERT INTO `educations` VALUES (2, 1, 'Bachelor Of Secondary Education', 1, '2024-02-28 02:29:15', '2024-02-28 02:29:15', NULL);

-- ----------------------------
-- Table structure for eligibilities
-- ----------------------------
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE `eligibilities`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `eligibility` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `eligibilities to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `eligibilities to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of eligibilities
-- ----------------------------
INSERT INTO `eligibilities` VALUES (1, 1, 'RA 1080', 1, '2024-02-28 02:29:35', '2024-02-28 02:29:35', NULL);

-- ----------------------------
-- Table structure for employments
-- ----------------------------
DROP TABLE IF EXISTS `employments`;
CREATE TABLE `employments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `teaching_type_id` bigint UNSIGNED NOT NULL,
  `employment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `year` year NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_per_year` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `employments to users`(`user_id` ASC) USING BTREE,
  INDEX `id`(`id` ASC) USING BTREE,
  INDEX `employments to teaching_types`(`teaching_type_id` ASC) USING BTREE,
  CONSTRAINT `employments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `employments to teaching_types` FOREIGN KEY (`teaching_type_id`) REFERENCES `teaching_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of employments
-- ----------------------------
INSERT INTO `employments` VALUES (2, 1, 1, 'PROFILING OF TEACHER | APPLICANTS FOR SY 2024', '<p>applications</p>', 2024, '2024-06-01', '2024-06-30', 0, 1, '2024-02-28 18:12:12', '2024-06-25 22:26:37', '0000-00-00 00:00:00');
INSERT INTO `employments` VALUES (3, 1, 2, 'PROFILING OF TEACHER | APPLICANTS FOR SY 2024', '<p>asdasdasd</p>', 2024, '2024-06-01', '2024-06-30', 0, 1, '2024-06-26 00:27:43', '2024-06-26 00:27:43', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for genders
-- ----------------------------
DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `genders to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `genders to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of genders
-- ----------------------------
INSERT INTO `genders` VALUES (1, 1, 'Straight', 1, 1, '2024-02-28 02:27:19', '2024-02-28 02:27:19', NULL);
INSERT INTO `genders` VALUES (2, 1, 'Lesbian', 2, 1, '2024-02-28 02:27:27', '2024-02-28 02:27:27', NULL);
INSERT INTO `genders` VALUES (3, 1, 'Gay', 3, 1, '2024-02-28 02:27:35', '2024-02-28 02:27:35', NULL);
INSERT INTO `genders` VALUES (4, 1, 'Bisexual', 4, 1, '2024-02-28 02:27:43', '2024-02-28 02:27:43', NULL);
INSERT INTO `genders` VALUES (5, 1, 'Transgender', 5, 1, '2024-02-28 02:27:53', '2024-02-28 02:27:53', NULL);
INSERT INTO `genders` VALUES (6, 1, 'Queer', 6, 1, '2024-02-28 02:28:06', '2024-02-28 02:28:06', NULL);
INSERT INTO `genders` VALUES (7, 1, 'Intersex', 7, 1, '2024-02-28 02:28:17', '2024-02-28 02:28:17', NULL);
INSERT INTO `genders` VALUES (8, 1, 'Asexual', 8, 1, '2024-02-28 02:28:27', '2024-02-28 02:28:27', NULL);
INSERT INTO `genders` VALUES (9, 1, 'Others', 9, 1, '2024-02-28 02:28:35', '2024-02-29 00:48:03', NULL);
INSERT INTO `genders` VALUES (10, 1, 'Ddd', 10, 1, '2024-02-29 00:47:55', '2024-02-29 00:47:55', '2024-02-28 16:47:58');

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `teaching_type_id` bigint UNSIGNED NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `positions to users`(`user_id` ASC) USING BTREE,
  INDEX `positions to teaching_types`(`teaching_type_id` ASC) USING BTREE,
  CONSTRAINT `positions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `positions to teaching_types` FOREIGN KEY (`teaching_type_id`) REFERENCES `teaching_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO `positions` VALUES (1, 1, 1, 'Kindergarten', 'KINDER', 1, '2024-02-28 02:30:13', '2024-06-15 19:30:19', NULL);
INSERT INTO `positions` VALUES (2, 1, 1, 'Elementary', 'ELEM', 1, '2024-02-28 02:30:26', '2024-02-28 02:30:26', NULL);
INSERT INTO `positions` VALUES (3, 1, 1, 'Junior High School', 'JHS', 1, '2024-02-28 02:30:42', '2024-02-28 02:30:42', NULL);
INSERT INTO `positions` VALUES (4, 1, 1, 'Senior High School', 'SHS', 1, '2024-02-28 02:30:53', '2024-02-28 02:30:53', NULL);
INSERT INTO `positions` VALUES (5, 1, 1, 'Asdasd', 'ASDASD', 1, '2024-06-25 23:23:26', '2024-06-25 23:23:26', '2024-06-25 15:23:29');
INSERT INTO `positions` VALUES (6, 1, 2, 'Teacher 1', 'ASDASDSA', 1, '2024-06-26 00:31:40', '2024-06-26 00:31:40', NULL);

-- ----------------------------
-- Table structure for religions
-- ----------------------------
DROP TABLE IF EXISTS `religions`;
CREATE TABLE `religions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `religion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `religions to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `religions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of religions
-- ----------------------------
INSERT INTO `religions` VALUES (1, 1, 'African Methodist Epsicopal Church', 1, '2024-02-28 03:01:58', '2024-02-28 03:01:58', NULL);
INSERT INTO `religions` VALUES (2, 1, 'Aglipayan (Philippine Independence Church)', 1, '2024-02-28 03:02:18', '2024-02-28 03:02:18', NULL);
INSERT INTO `religions` VALUES (3, 1, 'Agnostic', 1, '2024-02-28 03:02:26', '2024-02-28 03:02:26', NULL);
INSERT INTO `religions` VALUES (4, 1, 'American Baptist Church', 1, '2024-02-28 03:02:38', '2024-02-28 03:02:38', NULL);
INSERT INTO `religions` VALUES (5, 1, 'Anglican Church', 1, '2024-02-28 03:02:50', '2024-02-28 03:02:50', NULL);
INSERT INTO `religions` VALUES (6, 1, 'Apostolic Catholic Church', 1, '2024-02-28 03:03:08', '2024-02-28 03:03:08', NULL);
INSERT INTO `religions` VALUES (7, 1, 'Assemblies Of God', 1, '2024-02-28 03:03:18', '2024-02-28 03:03:18', NULL);
INSERT INTO `religions` VALUES (8, 1, 'Atheist (Did Not Identify Religion)', 1, '2024-02-28 03:03:39', '2024-02-28 03:03:39', NULL);
INSERT INTO `religions` VALUES (9, 1, 'Baptist World Alliance', 1, '2024-02-28 03:03:52', '2024-02-28 03:03:52', NULL);
INSERT INTO `religions` VALUES (10, 1, 'Bible Baptist', 1, '2024-02-28 03:04:01', '2024-02-28 03:04:12', NULL);
INSERT INTO `religions` VALUES (11, 1, 'Born Again Christian', 1, '2024-02-28 03:18:45', '2024-02-28 03:18:45', NULL);
INSERT INTO `religions` VALUES (12, 1, 'Buddhist', 1, '2024-02-28 03:19:26', '2024-02-28 03:19:26', NULL);
INSERT INTO `religions` VALUES (13, 1, 'Chinese', 1, '2024-02-28 03:19:43', '2024-02-28 03:19:43', NULL);
INSERT INTO `religions` VALUES (14, 1, 'Church Of God In Christ', 1, '2024-02-28 03:19:57', '2024-02-28 03:19:57', NULL);
INSERT INTO `religions` VALUES (15, 1, 'Church Of Jesust Christ Of Latter Day Saints', 1, '2024-02-28 03:20:23', '2024-02-28 03:20:23', NULL);
INSERT INTO `religions` VALUES (16, 1, 'Church Of The Nazarene', 1, '2024-02-28 03:20:37', '2024-02-28 03:20:37', NULL);
INSERT INTO `religions` VALUES (17, 1, 'Community Of Christ', 1, '2024-02-28 03:20:48', '2024-02-28 03:20:48', NULL);
INSERT INTO `religions` VALUES (18, 1, 'Convention Of The Philippine Baptist Church', 1, '2024-02-28 03:21:10', '2024-02-28 03:21:10', NULL);
INSERT INTO `religions` VALUES (19, 1, 'Crusaders Of The Divine Church Of Christ Inc', 1, '2024-02-28 03:21:36', '2024-02-28 03:21:36', NULL);
INSERT INTO `religions` VALUES (20, 1, 'El Shaddai', 1, '2024-02-28 03:21:48', '2024-02-28 03:21:48', NULL);
INSERT INTO `religions` VALUES (21, 1, 'Episcopal Church', 1, '2024-02-28 03:21:57', '2024-02-28 03:21:57', NULL);
INSERT INTO `religions` VALUES (22, 1, 'Evangelical', 1, '2024-02-28 03:22:06', '2024-02-28 03:22:06', NULL);
INSERT INTO `religions` VALUES (23, 1, 'Faith Tabernacle Church', 1, '2024-02-28 03:22:21', '2024-02-28 03:22:21', NULL);
INSERT INTO `religions` VALUES (24, 1, 'God World Mission Church', 1, '2024-02-28 03:22:37', '2024-02-28 03:22:37', NULL);
INSERT INTO `religions` VALUES (25, 1, 'Hinduism', 1, '2024-02-28 03:23:03', '2024-02-28 03:23:03', NULL);
INSERT INTO `religions` VALUES (26, 1, 'Iglesia Filipina Independiente', 1, '2024-02-28 03:23:22', '2024-02-28 03:23:22', NULL);
INSERT INTO `religions` VALUES (27, 1, 'Iglesia Ni Cristo (Church Of Christ)', 1, '2024-02-28 03:23:51', '2024-02-28 03:23:51', NULL);
INSERT INTO `religions` VALUES (28, 1, 'Iglesia Ng Dios Kay Cristo Jesus (Church Of God)', 1, '2024-02-28 03:24:25', '2024-02-28 03:24:25', NULL);
INSERT INTO `religions` VALUES (29, 1, 'Iglesia Sa Dios Espiritu Santo Inc', 1, '2024-02-28 03:24:46', '2024-02-28 03:24:46', NULL);
INSERT INTO `religions` VALUES (30, 1, 'Indigenous Religions', 1, '2024-02-28 03:25:01', '2024-02-28 03:25:01', NULL);
INSERT INTO `religions` VALUES (31, 1, 'Islam', 1, '2024-02-28 03:25:24', '2024-02-28 03:25:24', NULL);
INSERT INTO `religions` VALUES (32, 1, 'Jesus Is Lord Worldwide', 1, '2024-02-28 03:25:44', '2024-02-28 03:25:56', NULL);
INSERT INTO `religions` VALUES (33, 1, 'Jehovah\'s Witnesses', 1, '2024-02-28 03:27:38', '2024-02-28 03:27:38', NULL);
INSERT INTO `religions` VALUES (34, 1, 'Jesus Miracle Crusade', 1, '2024-02-28 03:27:52', '2024-02-28 03:27:52', NULL);
INSERT INTO `religions` VALUES (35, 1, 'Judaism', 1, '2024-02-28 03:30:38', '2024-02-28 03:30:38', NULL);
INSERT INTO `religions` VALUES (36, 1, 'Lutherian Church In The Philippines', 1, '2024-02-28 03:30:59', '2024-02-28 03:30:59', NULL);
INSERT INTO `religions` VALUES (37, 1, 'Members Church Of God International', 1, '2024-02-28 03:31:18', '2024-02-28 03:31:18', NULL);
INSERT INTO `religions` VALUES (38, 1, 'Mennonites', 1, '2024-02-28 03:31:36', '2024-02-28 03:31:45', NULL);
INSERT INTO `religions` VALUES (39, 1, 'Mount Banahaw Holy Conferederation', 1, '2024-02-28 03:32:38', '2024-02-28 03:32:38', NULL);
INSERT INTO `religions` VALUES (40, 1, 'Other Christian Denominations Combined', 1, '2024-02-28 03:33:55', '2024-02-28 03:33:55', NULL);
INSERT INTO `religions` VALUES (41, 1, 'Other Methodist', 1, '2024-02-28 03:34:18', '2024-02-28 03:34:18', NULL);
INSERT INTO `religions` VALUES (42, 1, 'Philippine Benevolent Missionary Association', 1, '2024-02-28 03:42:36', '2024-02-28 03:42:36', NULL);
INSERT INTO `religions` VALUES (43, 1, 'Philippine Episcopal Church', 1, '2024-02-28 03:44:52', '2024-02-28 03:44:52', NULL);
INSERT INTO `religions` VALUES (44, 1, 'Philippine Independent Church', 1, '2024-02-28 03:45:14', '2024-02-28 03:45:14', NULL);
INSERT INTO `religions` VALUES (45, 1, 'Philippine Orthodox Church', 1, '2024-02-28 03:45:30', '2024-02-28 03:45:30', NULL);
INSERT INTO `religions` VALUES (46, 1, 'Presbyterian', 1, '2024-02-28 03:45:41', '2024-02-28 03:45:41', NULL);
INSERT INTO `religions` VALUES (47, 1, 'Protestants', 1, '2024-02-28 03:45:56', '2024-02-28 03:45:56', NULL);
INSERT INTO `religions` VALUES (48, 1, 'Rizalistas', 1, '2024-02-28 03:46:05', '2024-02-28 03:46:05', NULL);
INSERT INTO `religions` VALUES (49, 1, 'Roman Catholic', 1, '2024-02-28 03:46:16', '2024-02-28 03:46:16', NULL);
INSERT INTO `religions` VALUES (50, 1, 'Seventh-Day Adventist', 1, '2024-02-28 03:46:30', '2024-02-28 03:46:30', NULL);
INSERT INTO `religions` VALUES (51, 1, 'Sikhism', 1, '2024-02-28 03:46:39', '2024-02-28 03:46:47', NULL);
INSERT INTO `religions` VALUES (52, 1, 'Unitatrian', 1, '2024-02-28 03:47:00', '2024-02-28 03:47:00', NULL);
INSERT INTO `religions` VALUES (53, 1, 'Southern Baptist Convention', 1, '2024-02-28 03:47:17', '2024-02-28 03:47:17', NULL);
INSERT INTO `religions` VALUES (54, 1, 'Taoism', 1, '2024-02-28 03:47:28', '2024-02-28 03:47:28', NULL);
INSERT INTO `religions` VALUES (55, 1, 'Union Espiritista Cristiana De Filipinas Inc', 1, '2024-02-28 03:48:29', '2024-02-28 03:48:29', NULL);
INSERT INTO `religions` VALUES (56, 1, 'Unitarian', 1, '2024-02-28 03:48:43', '2024-02-28 03:48:43', NULL);
INSERT INTO `religions` VALUES (57, 1, 'Unitarian Universalist', 1, '2024-02-28 03:49:02', '2024-02-28 03:49:02', NULL);
INSERT INTO `religions` VALUES (58, 1, 'United Church Of Christ In The Philippines', 1, '2024-02-28 03:49:41', '2024-02-28 03:49:41', NULL);
INSERT INTO `religions` VALUES (59, 1, 'United Methodist Church', 1, '2024-02-28 03:49:54', '2024-02-28 03:49:54', NULL);
INSERT INTO `religions` VALUES (60, 1, 'United Pentecostal Church', 1, '2024-02-28 03:50:09', '2024-02-28 03:50:09', NULL);
INSERT INTO `religions` VALUES (61, 1, 'Worldwide Church Of God', 1, '2024-02-28 03:50:24', '2024-02-28 03:50:24', NULL);

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013, 'CreateRememberMeTokens', '2024-02-27 14:52:25', '2024-02-27 14:52:25', 0);

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (7, '2024-06-26 00:15:15', '2024-06-26 00:15:15', 'Users', '1', '2a5d9c45baf99e601823502475f5413cad6b5b5c', '43729ae3de0ba36ee1e6743982dc24715c9b813f', '2024-07-26 00:15:15');

-- ----------------------------
-- Table structure for sexes
-- ----------------------------
DROP TABLE IF EXISTS `sexes`;
CREATE TABLE `sexes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sexes to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `sexes to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sexes
-- ----------------------------
INSERT INTO `sexes` VALUES (1, 1, 'Male', 1, '2024-02-28 02:26:44', '2024-02-28 02:26:44', NULL);
INSERT INTO `sexes` VALUES (2, 1, 'Female', 1, '2024-02-28 02:26:52', '2024-02-28 02:26:52', NULL);
INSERT INTO `sexes` VALUES (3, 1, 'Prefer Not To Say', 1, '2024-02-28 02:27:02', '2024-02-28 02:27:02', NULL);

-- ----------------------------
-- Table structure for suffixes
-- ----------------------------
DROP TABLE IF EXISTS `suffixes`;
CREATE TABLE `suffixes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `suffix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `suffixes to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `suffixes to users` FOREIGN KEY (`user_id`) REFERENCES `suffixes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of suffixes
-- ----------------------------
INSERT INTO `suffixes` VALUES (1, 1, 'JR', 1, '2024-06-15 20:04:47', '2024-06-15 20:04:47', NULL);
INSERT INTO `suffixes` VALUES (2, 1, 'SR', 1, '2024-06-15 20:04:51', '2024-06-15 20:04:51', NULL);
INSERT INTO `suffixes` VALUES (3, 1, 'III', 1, '2024-06-15 20:04:57', '2024-06-15 20:04:57', NULL);
INSERT INTO `suffixes` VALUES (4, 1, 'IV', 1, '2024-06-15 20:05:01', '2024-06-15 20:05:01', NULL);
INSERT INTO `suffixes` VALUES (5, 1, 'V', 1, '2024-06-15 20:05:04', '2024-06-15 20:05:04', NULL);
INSERT INTO `suffixes` VALUES (6, 1, 'NA', 1, '2024-06-15 20:05:25', '2024-06-15 20:05:37', NULL);

-- ----------------------------
-- Table structure for teachers
-- ----------------------------
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` bigint UNSIGNED NOT NULL,
  `teaching_type_id` bigint UNSIGNED NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `birthday` date NOT NULL,
  `age` int NOT NULL,
  `civil_status_id` bigint UNSIGNED NOT NULL,
  `sex_id` bigint UNSIGNED NOT NULL,
  `gender_id` bigint UNSIGNED NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `suffix_id` bigint UNSIGNED NOT NULL,
  `education_id` bigint UNSIGNED NOT NULL,
  `eligibility_id` bigint UNSIGNED NOT NULL,
  `is_non_teaching` tinyint NOT NULL,
  `position_id` bigint UNSIGNED NOT NULL,
  `barangay_id` bigint UNSIGNED NOT NULL,
  `religion_id` bigint UNSIGNED NOT NULL,
  `ethinicity` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `has_disability` tinyint UNSIGNED NOT NULL,
  `disability_id` bigint UNSIGNED NOT NULL,
  `employment_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teachers to employments`(`employment_id` ASC) USING BTREE,
  INDEX `teachers to civil_statuses`(`civil_status_id` ASC) USING BTREE,
  INDEX `teachers to sexes`(`sex_id` ASC) USING BTREE,
  INDEX `teachers to genders`(`gender_id` ASC) USING BTREE,
  INDEX `teachers to educations`(`education_id` ASC) USING BTREE,
  INDEX `teachers to eligibilities`(`eligibility_id` ASC) USING BTREE,
  INDEX `teachers to positions`(`position_id` ASC) USING BTREE,
  INDEX `teachers to barangays`(`barangay_id` ASC) USING BTREE,
  INDEX `teachers to religions`(`religion_id` ASC) USING BTREE,
  INDEX `teachers to disabilities`(`disability_id` ASC) USING BTREE,
  CONSTRAINT `teachers to barangays` FOREIGN KEY (`barangay_id`) REFERENCES `barangays` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to civil_statuses` FOREIGN KEY (`civil_status_id`) REFERENCES `civil_statuses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to disabilities` FOREIGN KEY (`disability_id`) REFERENCES `disabilities` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to educations` FOREIGN KEY (`education_id`) REFERENCES `educations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to eligibilities` FOREIGN KEY (`eligibility_id`) REFERENCES `eligibilities` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to employments` FOREIGN KEY (`employment_id`) REFERENCES `employments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to genders` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to religions` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teachers to sexes` FOREIGN KEY (`sex_id`) REFERENCES `sexes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teachers
-- ----------------------------
INSERT INTO `teachers` VALUES (1, 2, 1, 'cabotaje.rodrigo@gmail.com', 'Aaaa', 'RODS', '11', 'RODS', '2001-01-01', 23, 1, 3, 9, '09100575676', '-', 0, 1, 1, 1, 2, 0, 2, 'Asdadsa', 0, 21, 'SC-ELEM-0001', '667a8043e84cf0.95150700 1719304259', '2024-06-26 00:30:59', '2024-06-26 00:30:59', NULL);
INSERT INTO `teachers` VALUES (2, 3, 2, 'joppycalaunan@gmail.com', 'Asdasda', 'MARC JEFF', 'JEFF', 'JEFF', '2001-01-30', 23, 1, 1, 9, '09100575757', 'purok 3 taguinod street', 0, 2, 1, 0, 6, 0, 2, 'Sadasdas', 0, 21, 'SC-ASDASDSA-0001', '667a816c7c6bd0.50963200 1719304556', '2024-06-26 00:35:13', '2024-06-26 00:35:56', NULL);

-- ----------------------------
-- Table structure for teaching_types
-- ----------------------------
DROP TABLE IF EXISTS `teaching_types`;
CREATE TABLE `teaching_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `teaching_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_non_teaching` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teaching_types to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `teaching_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teaching_types
-- ----------------------------
INSERT INTO `teaching_types` VALUES (1, 1, 'NON-TEACHING', 1, 1, '2024-06-25 23:06:12', '2024-06-26 00:16:18', NULL);
INSERT INTO `teaching_types` VALUES (2, 1, 'TEACHING', 0, 1, '2024-06-26 00:16:15', '2024-06-26 00:16:15', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_admin` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$asROnynACVQBoGT9.KiFp.vgpAf/ND0P8LsnT8Z/xJRywJO2snCyu', 1, 1, '667a7c932b793667a7c932b796', '2024-02-27 23:01:44', '2024-06-26 00:15:15', NULL);

SET FOREIGN_KEY_CHECKS = 1;
