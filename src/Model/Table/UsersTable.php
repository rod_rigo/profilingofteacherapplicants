<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \App\Model\Table\BarangaysTable&\Cake\ORM\Association\HasMany $Barangays
 * @property \App\Model\Table\CivilStatusesTable&\Cake\ORM\Association\HasMany $CivilStatuses
 * @property \App\Model\Table\DisabilitiesTable&\Cake\ORM\Association\HasMany $Disabilities
 * @property \App\Model\Table\EducationsTable&\Cake\ORM\Association\HasMany $Educations
 * @property \App\Model\Table\EligibilitiesTable&\Cake\ORM\Association\HasMany $Eligibilities
 * @property \App\Model\Table\EmploymentsTable&\Cake\ORM\Association\HasMany $Employments
 * @property \App\Model\Table\GendersTable&\Cake\ORM\Association\HasMany $Genders
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\HasMany $Positions
 * @property \App\Model\Table\ReligionsTable&\Cake\ORM\Association\HasMany $Religions
 * @property \App\Model\Table\SexesTable&\Cake\ORM\Association\HasMany $Sexes
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Barangays', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('CivilStatuses', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Disabilities', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Educations', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Eligibilities', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Employments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Genders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Religions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Sexes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Suffixes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('TeachingTypes', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', true)
            ->notEmptyString('username', ucwords('please fill out this field'),false);

        $validator
            ->email('email')
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password', ucwords('please fill out this field'),true);

        $validator
            ->numeric('is_admin')
            ->notEmptyString('is_admin', ucwords('please select is admin'), false)
            ->add('is_admin','is_admin', [
                'rule' => function($value){

                    $isAdmin = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isAdmin)){

                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active', ucwords('please select is active'), false)
            ->add('is_active','is_active', [
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){

                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->requirePresence('token', true)
            ->notEmptyString('token', ucwords('please fill out this field'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'users.id' => $userId]);
    }

    public function findAuth(Query $query, array $options)
    {
        $query->where([
            'OR' => [ //<-- we use OR operator for our SQL
                'username' => $options['username'], //<-- username column
                'email' => $options['username'] //<-- email column
            ],
            'is_active =' =>  intval(1)], [], true);

        return $query;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }
}
