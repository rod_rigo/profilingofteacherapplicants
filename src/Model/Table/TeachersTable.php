<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function Sodium\add;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Teachers Model
 *
 * @property \App\Model\Table\EmploymentsTable&\Cake\ORM\Association\BelongsTo $Employments
 * @property \App\Model\Table\CivilStatusesTable&\Cake\ORM\Association\BelongsTo $CivilStatuses
 * @property \App\Model\Table\SexesTable&\Cake\ORM\Association\BelongsTo $Sexes
 * @property \App\Model\Table\GendersTable&\Cake\ORM\Association\BelongsTo $Genders
 * @property \App\Model\Table\EducationsTable&\Cake\ORM\Association\BelongsTo $Educations
 * @property \App\Model\Table\EligibilitiesTable&\Cake\ORM\Association\BelongsTo $Eligibilities
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\BelongsTo $Positions
 * @property \App\Model\Table\BarangaysTable&\Cake\ORM\Association\BelongsTo $Barangays
 * @property \App\Model\Table\ReligionsTable&\Cake\ORM\Association\BelongsTo $Religions
 * @property \App\Model\Table\DisabilitiesTable&\Cake\ORM\Association\BelongsTo $Disabilities
 *
 * @method \App\Model\Entity\Teacher newEmptyEntity()
 * @method \App\Model\Entity\Teacher newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Teacher[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Teacher get($primaryKey, $options = [])
 * @method \App\Model\Entity\Teacher findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Teacher patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Teacher[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Teacher|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Teacher saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Teacher[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Teacher[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Teacher[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Teacher[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TeachersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('teachers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employments', [
            'foreignKey' => 'employment_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('CivilStatuses', [
            'foreignKey' => 'civil_status_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Sexes', [
            'foreignKey' => 'sex_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Educations', [
            'foreignKey' => 'education_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Eligibilities', [
            'foreignKey' => 'eligibility_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Positions', [
            'foreignKey' => 'position_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Barangays', [
            'foreignKey' => 'barangay_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Religions', [
            'foreignKey' => 'religion_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Disabilities', [
            'foreignKey' => 'disability_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Suffixes', [
            'foreignKey' => 'suffix_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('TeachingTypes', [
            'foreignKey' => 'teaching_type_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('employment_id')
            ->requirePresence('employment_id',true)
            ->notEmptyString('employment_id', ucwords('please enter a employment id'),false);

        $validator
            ->numeric('teaching_type_id')
            ->requirePresence('teaching_type_id',true)
            ->notEmptyString('teaching_type_id', ucwords('please enter a teaching-Type id'),false);

        $validator
            ->numeric('civil_status_id')
            ->requirePresence('civil_status_id',true)
            ->notEmptyString('civil_status_id', ucwords('please enter a civil status id'),false);

        $validator
            ->numeric('sex_id')
            ->requirePresence('sex_id',true)
            ->notEmptyString('sex_id', ucwords('please enter a sex id'),false);

        $validator
            ->numeric('suffix_id')
            ->requirePresence('suffix_id',true)
            ->notEmptyString('suffix_id', ucwords('please enter a suffix id'),false);

        $validator
            ->numeric('gender_id')
            ->requirePresence('gender_id',true)
            ->notEmptyString('gender_id', ucwords('please enter a gender id'),false);

        $validator
            ->numeric('education_id')
            ->requirePresence('education_id',true)
            ->notEmptyString('education_id', ucwords('please enter a education id'),false);

        $validator
            ->numeric('eligibility_id')
            ->requirePresence('eligibility_id',true)
            ->notEmptyString('eligibility_id', ucwords('please enter a eligibility id'),false);

        $validator
            ->numeric('position_id')
            ->requirePresence('position_id',true)
            ->notEmptyString('position_id', ucwords('please enter a position id'),false);

        $validator
            ->numeric('barangay_id')
            ->requirePresence('barangay_id',true)
            ->notEmptyString('barangay_id', ucwords('please enter a barangay id'),false);

        $validator
            ->numeric('religion_id')
            ->requirePresence('religion_id',true)
            ->notEmptyString('religion_id', ucwords('please enter a religion id'),false);

        $validator
            ->numeric('disability_id')
            ->requirePresence('disability_id',true)
            ->notEmptyString('disability_id', ucwords('please enter a disability id'),false);

        $validator
            ->requirePresence('no', true)
            ->numeric('no')
            ->notEmptyString('no', ucwords('please fill out this field'),false);

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', true)
            ->notEmptyString('code', ucwords('please fill out this field'),false);

        $validator
            ->email('email')
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('please fill out this field'),false)
            ->add('email','email',[
                'rule' => function($value){

                    if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
                        return ucwords('please enter a valid email');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('answer')
            ->maxLength('answer', 4294967295)
            ->requirePresence('answer', true)
            ->notEmptyString('answer', ucwords('please fill out this field'),false);

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', true)
            ->notEmptyString('first_name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 255)
            ->requirePresence('middle_name', true)
            ->allowEmptyString('middle_name', ucwords('please fill out this field'),true);

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', true)
            ->notEmptyString('last_name', ucwords('please fill out this field'),false);

        $validator
            ->date('birthday')
            ->requirePresence('birthday', true)
            ->notEmptyDate('birthday', ucwords('please fill out this field'),false);

        $validator
            ->integer('age')
            ->requirePresence('age', true)
            ->notEmptyString('age', ucwords('please fill out this field'),false);

        $validator
            ->scalar('contact_number')
            ->maxLength('contact_number', 255)
            ->requirePresence('contact_number', true)
            ->notEmptyString('contact_number', ucwords('please fill out this field'),false)
            ->add('contact_number', 'contact_number', [
                'rule' => function($value){

                    if(!preg_match_all('/^(09)([0-9]{9})$/',$value)){
                        return ucwords('contact number must be start with  0 & 9 followed by 9 digits');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('address')
            ->maxLength('address', 4294967295)
            ->requirePresence('address', true)
            ->notEmptyString('address', ucwords('please fill out this field'),false);

        $validator
            ->numeric('is_non_teaching')
            ->notEmptyString('is_non_teaching', ucwords('please select is non teaching value'), false)
            ->add('is_non_teaching','is_non_teaching', [
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('Invalid is active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('ethinicity')
            ->maxLength('ethinicity', 4294967295)
            ->requirePresence('ethinicity', true)
            ->notEmptyString('ethinicity', ucwords('please fill out this field'),false);

        $validator
            ->requirePresence('has_disability', true)
            ->notEmptyString('has_disability', ucwords('please fill out this field'),false);

        $validator
            ->scalar('employment_code')
            ->maxLength('employment_code', 255)
            ->requirePresence('employment_code', true)
            ->notEmptyString('employment_code', ucwords('please fill out this field'),false);

        $validator
            ->numeric('is_non_teaching')
            ->notEmptyString('is_non_teaching', ucwords('please select is non teaching value'), false)
            ->add('is_non_teaching','is_non_teaching', [
                'rule' => function($value){

                    $isNonTeaching = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isNonTeaching)){
                        return ucwords('Invalid is non teaching value');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_transfered')
            ->notEmptyString('is_transfered', ucwords('please select is transfered value'), false)
            ->add('is_transfered','is_transfered', [
                'rule' => function($value){

                    $isTransfered = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isTransfered)){
                        return ucwords('Invalid is transfered value');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_occupied')
            ->notEmptyString('is_occupied', ucwords('please select is occupied value'), false)
            ->add('is_occupied','is_occupied', [
                'rule' => function($value){

                    $isOccupied = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isOccupied)){
                        return ucwords('Invalid is occupied value');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->requirePresence('token', true)
            ->notEmptyString('token', ucwords('please fill out this field'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['teaching_type_id'], 'TeachingTypes'), ['errorField' => 'teaching_type_id']);
        $rules->add($rules->existsIn(['employment_id'], 'Employments'), ['errorField' => 'employment_id']);
        $rules->add($rules->existsIn(['civil_status_id'], 'CivilStatuses'), ['errorField' => 'civil_status_id']);
        $rules->add($rules->existsIn(['sex_id'], 'Sexes'), ['errorField' => 'sex_id']);
        $rules->add($rules->existsIn(['gender_id'], 'Genders'), ['errorField' => 'gender_id']);
        $rules->add($rules->existsIn(['education_id'], 'Educations'), ['errorField' => 'education_id']);
        $rules->add($rules->existsIn(['eligibility_id'], 'Eligibilities'), ['errorField' => 'eligibility_id']);
        $rules->add($rules->existsIn(['position_id'], 'Positions'), ['errorField' => 'position_id']);
        $rules->add($rules->existsIn(['barangay_id'], 'Barangays'), ['errorField' => 'barangay_id']);
        $rules->add($rules->existsIn(['religion_id'], 'Religions'), ['errorField' => 'religion_id']);
        $rules->add($rules->existsIn(['disability_id'], 'Disabilities'), ['errorField' => 'disability_id']);
        $rules->add($rules->existsIn(['suffix_id'], 'Suffixes'), ['errorField' => 'suffix_id']);
        $rules->add($rules->isUnique(['email', 'employment_id', 'is_occupied'], ucwords('This email is already used in this activity')), ['errorField' => 'email']);
        $rules->add($rules->isUnique(['contact_number', 'employment_id', 'is_occupied'], ucwords('This contact number is already used in this activity')), ['errorField' => 'contact_number']);
        $rules->add($rules->isUnique(['first_name', 'middle_name', 'last_name', 'email','contact_number', 'employment_id', 'is_occupied'], ucwords('this name is already registered to this activity')), ['errorField' => 'first_name']);
        $rules->add($rules->isUnique(['first_name', 'middle_name', 'last_name', 'email','contact_number', 'employment_id', 'is_occupied'], ucwords('this name is already registered to this activity')), ['errorField' => 'middle_name']);
        $rules->add($rules->isUnique(['first_name', 'middle_name', 'last_name', 'email','contact_number', 'employment_id', 'is_occupied'], ucwords('this name is already registered to this activity')), ['errorField' => 'last_name']);

        return $rules;
    }
}
