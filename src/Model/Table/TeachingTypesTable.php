<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * TeachingTypes Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\EmploymentsTable&\Cake\ORM\Association\HasMany $Employments
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\HasMany $Positions
 * @property \App\Model\Table\TeachersTable&\Cake\ORM\Association\HasMany $Teachers
 *
 * @method \App\Model\Entity\TeachingType newEmptyEntity()
 * @method \App\Model\Entity\TeachingType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\TeachingType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TeachingType get($primaryKey, $options = [])
 * @method \App\Model\Entity\TeachingType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\TeachingType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TeachingType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\TeachingType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TeachingType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TeachingType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TeachingType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\TeachingType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TeachingType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TeachingTypesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('teaching_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Employments', [
            'foreignKey' => 'teaching_type_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'teaching_type_id',
        ]);
        $this->hasMany('Teachers', [
            'foreignKey' => 'teaching_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('teaching_type')
            ->maxLength('teaching_type', 255)
            ->requirePresence('teaching_type', true)
            ->notEmptyString('teaching_type', ucwords('please fill out this field'), false);

        $validator
            ->numeric('is_non_teaching')
            ->notEmptyString('is_non_teaching', ucwords('please select is non teaching value'), false)
            ->add('is_non_teaching','is_non_teaching', [
                'rule' => function($value){

                    $isNonTeaching = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isNonTeaching)){
                        return ucwords('Invalid is non teaching value');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active', ucwords('please select is non teaching value'), false)
            ->add('is_active','is_active', [
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){

                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['teaching_type']), ['errorField' => 'teaching_type']);

        return $rules;
    }
}
