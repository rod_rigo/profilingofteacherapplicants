<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Suffixes Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Suffix newEmptyEntity()
 * @method \App\Model\Entity\Suffix newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Suffix[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Suffix get($primaryKey, $options = [])
 * @method \App\Model\Entity\Suffix findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Suffix patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Suffix[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Suffix|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Suffix saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Suffix[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Suffix[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Suffix[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Suffix[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SuffixesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('suffixes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Teachers', [
            'foreignKey' => 'suffix_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('suffix')
            ->maxLength('suffix', 255)
            ->requirePresence('suffix', true)
            ->notEmptyString('suffix', ucwords('please fill out this field'), false);

        $validator
            ->numeric('is_shown')
            ->notEmptyString('is_shown', ucwords('please select is shown'), false)
            ->add('is_shown','is_shown', [
                'rule' => function($value){

                    $isShown = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isShown)){
                        return ucwords('please select is shown');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active', ucwords('please select is active'), false)
            ->add('is_active','is_active', [
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('please select is active');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('order_position')
            ->notEmptyString('order_position', ucwords('please fill out this field'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['suffix'], ucwords('the suffix is already exists')), ['errorField' => 'suffix']);

        return $rules;
    }
}
