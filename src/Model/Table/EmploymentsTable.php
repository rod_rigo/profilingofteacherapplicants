<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Employments Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TeachersTable&\Cake\ORM\Association\HasMany $Teachers
 *
 * @method \App\Model\Entity\Employment newEmptyEntity()
 * @method \App\Model\Entity\Employment newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Employment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Employment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Employment findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Employment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Employment[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Employment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Employment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Employment[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Employment[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Employment[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Employment[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmploymentsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('employments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Teachers', [
            'foreignKey' => 'employment_id',
        ]);
        $this->belongsTo('TeachingTypes', [
            'foreignKey' => 'teaching_type_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('teaching_type_id')
            ->requirePresence('teaching_type_id',true)
            ->notEmptyString('teaching_type_id', ucwords('please enter a teaching-Type id'),false);

        $validator
            ->scalar('employment')
            ->maxLength('employment', 255)
            ->requirePresence('employment', true)
            ->notEmptyString('employment', ucwords('please fill out this field'), false);

        $validator
            ->scalar('description')
            ->maxLength('description', 4294967295)
            ->requirePresence('description', true)
            ->notEmptyString('description', ucwords('please enter a description'), false);

        $validator
            ->scalar('start_date')
            ->date('start_date')
            ->requirePresence('start_date',true)
            ->notEmptyDate('start_date', ucwords('please fill out this field'),false);

        $validator
            ->scalar('end_date')
            ->date('end_date')
            ->requirePresence('end_date',true)
            ->notEmptyDate('end_date', ucwords('please fill out this field'),false);

        $validator
            ->scalar('year')
            ->requirePresence('year', true)
            ->notEmptyString('year', ucwords('please enter a description'), false);

        $validator
            ->numeric('is_per_year')
            ->notEmptyString('is_per_year', ucwords('please select is per year '), false)
            ->add('is_per_year','is_per_year', [
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('Invalid is per year value');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active', ucwords('please select is active'), false)
            ->add('is_active','is_active', [
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('Invalid is active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['teaching_type_id'], 'TeachingTypes'), ['errorField' => 'teaching_type_id']);
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['teaching_type_id', 'employment'], ucwords('this value is already exists in this Teaching-Type')), ['errorField' => 'employment']);

        return $rules;
    }
}
