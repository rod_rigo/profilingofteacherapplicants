<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Disabilities Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TeachersTable&\Cake\ORM\Association\HasMany $Teachers
 *
 * @method \App\Model\Entity\Disability newEmptyEntity()
 * @method \App\Model\Entity\Disability newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Disability[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Disability get($primaryKey, $options = [])
 * @method \App\Model\Entity\Disability findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Disability patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Disability[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Disability|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Disability saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Disability[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Disability[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Disability[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Disability[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DisabilitiesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('disabilities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Teachers', [
            'foreignKey' => 'disability_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('disability')
            ->maxLength('disability', 255)
            ->requirePresence('disability', true)
            ->notEmptyString('disability', ucwords('please fill out this field'),false);

        $validator
            ->numeric('position')
            ->requirePresence('position', true)
            ->notEmptyString('position', ucwords('please enter position '), false);

        $validator
            ->numeric('is_no')
            ->notEmptyString('is_no', ucwords('please select is no'), false)
            ->add('is_no','is_no', [
                'rule' => function($value){

                    $isActive = [0, 1];
                    if(!in_array(intval($value), $isActive)){

                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active', ucwords('please select is active'), false)
            ->add('is_active','is_active', [
                'rule' => function($value){

                    $isActive = [0, 1];
                    if(!in_array(intval($value), $isActive)){

                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['disability'], ucwords('this value is already exists')), ['errorField' => 'disability']);

        return $rules;
    }
}
