<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Teacher Entity
 *
 * @property int $id
 * @property int $employment_id
 * @property int $teaching_type_id
 * @property int $no
 * @property string $code
 * @property string $email
 * @property string $answer
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property \Cake\I18n\FrozenDate $birthday
 * @property int $age
 * @property int $civil_status_id
 * @property int $sex_id
 * @property int $gender_id
 * @property string $contact_number
 * @property string $address
 * @property int $suffix_id
 * @property int $education_id
 * @property int $eligibility_id
 * @property int $is_non_teaching
 * @property int $position_id
 * @property int $barangay_id
 * @property int $religion_id
 * @property string $ethinicity
 * @property int $has_disability
 * @property int $disability_id
 * @property string $employment_code
 * @property int $is_transfered
 * @property int $is_occupied
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Employment $employment
 * @property \App\Model\Entity\TeachingType $teaching_type
 * @property \App\Model\Entity\CivilStatus $civil_status
 * @property \App\Model\Entity\Sex $sex
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\Education $education
 * @property \App\Model\Entity\Eligibility $eligibility
 * @property \App\Model\Entity\Position $position
 * @property \App\Model\Entity\Barangay $barangay
 * @property \App\Model\Entity\Religion $religion
 * @property \App\Model\Entity\Disability $disability
 * @property \App\Model\Entity\Suffix $suffix
 */
class Teacher extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'employment_id' => true,
        'teaching_type_id' => true,
        'no' => true,
        'code' => true,
        'email' => true,
        'answer' => true,
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'birthday' => true,
        'age' => true,
        'civil_status_id' => true,
        'sex_id' => true,
        'gender_id' => true,
        'contact_number' => true,
        'address' => true,
        'suffix_id' => true,
        'education_id' => true,
        'eligibility_id' => true,
        'is_non_teaching' => true,
        'position_id' => true,
        'barangay_id' => true,
        'religion_id' => true,
        'ethinicity' => true,
        'has_disability' => true,
        'disability_id' => true,
        'employment_code' => true,
        'is_transfered' => true,
        'is_occupied' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'employment' => true,
        'civil_status' => true,
        'sex' => true,
        'gender' => true,
        'education' => true,
        'eligibility' => true,
        'position' => true,
        'barangay' => true,
        'religion' => true,
        'disability' => true,
        'suffix' => true,
        'teaching_type' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token',
    ];

    protected function _setCode($value){
        return strtoupper($value);
    }

    protected function _setFirstName($value){
        return strtoupper($value);
    }

    protected function _setMiddleName($value){
        return (intval(strlen($value)) > intval(0))? strtoupper($value): '';
    }

    protected function _setLastName($value){
        return strtoupper($value);
    }

    protected function _setAnswer($value){
        return ucwords($value);
    }

    protected function _setEthinicity($value){
        return ucwords($value);
    }

}
