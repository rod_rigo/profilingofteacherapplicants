<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Employment Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $teaching_type_id
 * @property string $employment
 * @property string $description
 * @property \Cake\I18n\FrozenDate $start_date
 * @property \Cake\I18n\FrozenDate $end_date
 * @property string $year
 * @property int $is_per_year
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\TeachingType $teaching_type
 * @property \App\Model\Entity\Teacher[] $teachers
 */
class Employment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'teaching_type_id' => true,
        'employment' => true,
        'description' => true,
        'start_date' => true,
        'end_date' => true,
        'year' => true,
        'is_per_year' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'teachers' => true,
        'teaching_type' => true,
    ];

    protected function _setEmployment($value){
        return strtoupper($value);
    }

}
