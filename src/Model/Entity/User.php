<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property int $is_admin
 * @property int $is_active
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Barangay[] $barangays
 * @property \App\Model\Entity\CivilStatus[] $civil_statuses
 * @property \App\Model\Entity\Disability[] $disabilities
 * @property \App\Model\Entity\Education[] $educations
 * @property \App\Model\Entity\Eligibility[] $eligibilities
 * @property \App\Model\Entity\Employment[] $employments
 * @property \App\Model\Entity\Gender[] $genders
 * @property \App\Model\Entity\Position[] $positions
 * @property \App\Model\Entity\Religion[] $religions
 * @property \App\Model\Entity\Sex[] $sexes
 * @property \App\Model\Entity\Suffix[] $suffixes
 * @property \App\Model\Entity\TeachingType[] $teaching_types
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'username' => true,
        'email' => true,
        'password' => true,
        'is_admin' => true,
        'is_active' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'barangays' => true,
        'civil_statuses' => true,
        'disabilities' => true,
        'educations' => true,
        'eligibilities' => true,
        'employments' => true,
        'genders' => true,
        'positions' => true,
        'religions' => true,
        'sexes' => true,
        'suffixes' => true,
        'teaching_types' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword($value){
        return (new DefaultPasswordHasher())->hash($value);
    }

}
