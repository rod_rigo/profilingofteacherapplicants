<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Barangay Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $barangay
 * @property int $is_defined
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Teacher[] $teachers
 */
class Barangay extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'barangay' => true,
        'is_defined' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'teachers' => true,
    ];

    protected function _setBarangay($value){
        return ucwords($value);
    }

}
