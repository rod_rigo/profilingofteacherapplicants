<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Moment\Moment;

/**
 * Teachers Controller
 *
 * @property \App\Model\Table\TeachersTable $Teachers
 * @method \App\Model\Entity\Teacher[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TeachersController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->allow();
        $this->viewBuilder()->setLayout('teacher');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index($employmentId = null)
    {
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $teacher = $this->Teachers->newEmptyEntity();
        $employment = $this->Teachers->Employments->get($employmentId,[
            'contain' => [
                'TeachingTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ],
            'conditions' => [
                'OR' => [
                    [
                        'Employments.start_date <=' => $now,
                        'Employments.end_date >=' => $now,
                    ],
                    [
                        'Employments.is_per_year =' => intval(1),
                    ],
                ],
                'Employments.is_active =' => intval(1)
            ]
        ]);
        $civilStatuses = $this->Teachers->CivilStatuses->find('list', [
                'valueField' => function($query){
                    return ucwords($query->civil_status);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'CivilStatuses.is_active =' => intval(1)
            ])->order(['CivilStatuses.civil_status' => 'ASC'],true);
        $sexes = $this->Teachers->Sexes->find('list', [
                'valueField' => function($query){
                    return ucwords($query->sex);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Sexes.is_active =' => intval(1)
            ])->order(['Sexes.order_position' => 'ASC'],true);
        $genders = $this->Teachers->Genders->find('list', [
                'valueField' => function($query){
                    return ucwords($query->gender);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Genders.is_active =' => intval(1)
            ])->order(['position' => 'ASC'],true);
        $educations = $this->Teachers->Educations->find()
            ->where([
                'Educations.is_active =' => intval(1),
                'Educations.is_default =' => intval(1)
            ])
            ->order(['Educations.education' => 'ASC'],true)
            ->first();
        $eligibilities = $this->Teachers->Eligibilities->find('list', [
                'valueField' => function($query){
                    return ucwords($query->eligibility);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Eligibilities.is_active =' => intval(1)
            ])->order(['eligibility' => 'ASC'],true);
        $barangays = $this->Teachers->Barangays->find('list', [
                'valueField' => function($query){
                    return ucwords($query->barangay);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Barangays.is_active =' => intval(1),
                'Barangays.is_defined =' => intval($employment->teaching_type->is_non_teaching)
            ])->order(['Barangays.barangay' => 'ASC'],true);
        $religions = $this->Teachers->Religions->find('list', [
                'valueField' => function($query){
                    return ucwords($query->religion);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Religions.is_active =' => intval(1)
            ])->order(['Religions.religion' => 'ASC'],true);
        $disabilities = $this->Teachers->Disabilities->find()
            ->select([
                'id',
                'disability',
                'is_no'
            ])
            ->where([
                'Disabilities.is_active =' => intval(1)
            ])
            ->order(['Disabilities.position' => 'ASC'],true);
        $suffixes = TableRegistry::getTableLocator()->get('Suffixes')
            ->find('list', [
                'valueField' => function($query){
                    return strtoupper($query->suffix);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Suffixes.is_active =' => intval(1)
            ])->order(['Suffixes.order_position' => 'ASC'],true);
            $positions = TableRegistry::getTableLocator()->get('Positions')->find('list', [
                'valueField' => function($query){
                    return ucwords($query->position);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Positions.teaching_type_id =' => intval($employment->teaching_type_id),
                'Positions.is_active =' => intval(1)
            ])
            ->order(['position' => 'ASC'],true);
        $this->set(compact('teacher', 'employment', 'civilStatuses', 'sexes', 'genders', 'educations', 'eligibilities', 'barangays', 'religions', 'disabilities', 'suffixes', 'positions'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $teacher = $this->Teachers->newEmptyEntity();
        if ($this->request->is('post')) {
            $connection = ConnectionManager::get('default');
            $connection->begin();
            $connection->query('SET FOREIGN_KEY_CHECKS=0;');

            $isTeacherOccupied = $this->Teachers->find()
                ->where([
                    'Teachers.position_id =' => intval($this->request->getData('position_id')),
                    'Teachers.employment_id =' => intval($this->request->getData('employment_id')),
                    'Teachers.is_transfered =' => intval(0),
                    'Teachers.is_occupied =' => intval(0),
                ])
                ->disableHydration()
                ->first();

            try{

                if(!empty($isTeacherOccupied)){

                    $entity = $this->Teachers->get($isTeacherOccupied['id']);
                    $no = intval($entity['no']);
                    $code = strval($entity['code']);
                    $employmentCode = strval($entity['employment_code']);
                    $positionId = intval($entity['position_id']);

                    $entity = $this->Teachers->patchEntity($entity, $this->request->getData(),[
                        'accessibleFields' =>[
                            'civil_status' => false,
                            'sex' => false,
                            'gender' => false,
                            'education' => false,
                            'eligibility' => false,
                            'position' => false,
                            'barangay' => false,
                            'religion' => false,
                            'disability' => false,
                            'suffix' => false,
                        ]
                    ]);

                    $validator = new Validator();

                    $validator
                        ->maxLength('education', 255)
                        ->notEmptyString('education', ucwords('please fill out this field'),false)
                        ->add('education','education', [
                            'rule' => function($value){

                                if(intval(strlen($value)) < intval(6)){
                                    return ucwords('please do not use abbreviation');
                                }

                                return true;
                            }
                        ]);

                    $errors = $validator->validate($this->request->getData());

                    foreach ($errors as $key => $value){
                        $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                            'errors' => $errors];
                        return $this->response->withStatus(422)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }

                    $education = TableRegistry::getTableLocator()->get('Educations')
                        ->find()
                        ->where([
                            'Educations.education like' => '%'.(ucwords($this->request->getData('education'))).'%',
                            'Educations.is_active =' => intval(1),
                        ])
                        ->first();

                    if(!empty($education)){
                        $entity->education_id = intval($education->id);
                    }else{
                        $education = TableRegistry::getTableLocator()->get('Educations')
                            ->newEmptyEntity();
                        $education = TableRegistry::getTableLocator()->get('Educations')
                            ->patchEntity($education, [
                                'user_id' => intval(1),
                                'education' => ucwords($this->request->getData('education')),
                                'is_active' => intval(1)
                            ]);
                        if(TableRegistry::getTableLocator()->get('Educations')->save($education)){
                            $entity->education_id = intval($education->id);
                        }

                    }

                    $entity->no = intval($no);
                    $entity->code = strval($code);
                    $entity->employment_code = strval($employmentCode);
                    $entity->is_transfered = intval(0);
                    $entity->is_occupied = intval(1);
                    $entity->position_id = intval($positionId);
                    $entity->token = uniqid().microtime();

                    if ($this->Teachers->save($entity)) {
                        $connection->commit();
                        $result = ['message' => ucwords('your application has been submitted'), 'result' => ucwords('success'),
                            'redirect' => Router::url(['prefix' => false, 'controller' => 'Teachers', 'action' => 'view', base64_encode(strval($entity->id))])];
                        return $this->response->withStatus(200)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }else{
                        foreach ($teacher->getErrors() as $key => $value){
                            $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                                'errors' => $teacher->getErrors()];
                            return $this->response->withStatus(422)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                    }

                }else{
                    $teacher = $this->Teachers->patchEntity($teacher, $this->request->getData(),[
                        'accessibleFields' =>[
                            'civil_status' => false,
                            'sex' => false,
                            'gender' => false,
                            'education' => false,
                            'eligibility' => false,
                            'position' => false,
                            'barangay' => false,
                            'religion' => false,
                            'disability' => false,
                            'suffix' => false,
                        ]
                    ]);

                    $position = TableRegistry::getTableLocator()->get('Positions')->get(intval($this->request->getData('position_id')));

                    $no = @$this->Teachers->find()
                        ->where([
                            'Teachers.position_id =' => intval($position->id)
                        ])
                        ->max('no')
                        ->no;

                    $validator = new Validator();

                    $validator
                        ->maxLength('education', 255)
                        ->notEmptyString('education', ucwords('please fill out this field'),false)
                        ->add('education','education', [
                            'rule' => function($value){

                                if(intval(strlen($value)) < intval(6)){
                                    return ucwords('please do not use abbreviation');
                                }

                                return true;
                            }
                        ]);

                    $errors = $validator->validate($this->request->getData());

                    foreach ($errors as $key => $value){
                        $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                            'errors' => $errors];
                        return $this->response->withStatus(422)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }

                    $education = TableRegistry::getTableLocator()->get('Educations')
                        ->find()
                        ->where([
                            'Educations.education like' => '%'.(ucwords($this->request->getData('education'))).'%',
                            'Educations.is_active =' => intval(1),
                        ])
                        ->first();

                    if(!empty($education)){
                        $teacher->education_id = intval($education->id);
                    }else{
                        $education = TableRegistry::getTableLocator()->get('Educations')
                            ->newEmptyEntity();
                        $education = TableRegistry::getTableLocator()->get('Educations')
                            ->patchEntity($education, [
                                'user_id' => intval(1),
                                'education' => ucwords($this->request->getData('education')),
                                'is_active' => intval(1)
                            ]);
                        if(TableRegistry::getTableLocator()->get('Educations')->save($education)){
                            $teacher->education_id = intval($education->id);
                        }

                    }

                    $teacher->no = (intval(@$no) + intval(1));
                    $teacher->code = strval($position->code);
                    $teacher->is_transfered = intval(0);
                    $teacher->is_occupied = intval(1);
                    $teacher->employment_code = strtoupper('SC-'.($position->code)).'-'.(str_pad(strval((intval(@$no) + intval(1))), 4, '0', STR_PAD_LEFT));
                    $teacher->token = uniqid().microtime();

                    if ($this->Teachers->save($teacher)) {
                        $connection->commit();
                        $result = ['message' => ucwords('your application has been submitted'), 'result' => ucwords('success'),
                            'redirect' => Router::url(['prefix' => false, 'controller' => 'Teachers', 'action' => 'view', base64_encode(strval($teacher->id))])];
                        return $this->response->withStatus(200)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }else{
                        foreach ($teacher->getErrors() as $key => $value){
                            $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                                'errors' => $teacher->getErrors()];
                            return $this->response->withStatus(422)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                    }
                }

            }catch (\Exception $exception){
                $connection->rollback();
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
    }

    public function view($id = null){
        $teacher = $this->Teachers->get(base64_decode($id),[
            'contain' => [
                'Employments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]
        ]);
        $this->set(compact('teacher'));
    }
    /**
     * Edit method
     *
     * @param string|null $id Teacher id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $teacher = $this->Teachers->get(base64_decode($id), [
            'contain' => [
                'Educations' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $connection = ConnectionManager::get('default');
            $connection->begin();
            try{
                $validator = new Validator();

                $validator
                    ->maxLength('education', 255)
                    ->notEmptyString('education', ucwords('please fill out this field'),false)
                    ->add('education','education', [
                        'rule' => function($value){

                            if(intval(strlen($value)) < intval(6)){
                                return ucwords('please do not use abbreviation');
                            }

                            return true;
                        }
                    ]);

                $errors = $validator->validate($this->request->getData());

                foreach ($errors as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $errors];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
                $teacher = $this->Teachers->patchEntity($teacher, $this->request->getData(),[
                    'accessibleFields' =>[
                        'civil_status' => false,
                        'sex' => false,
                        'gender' => false,
                        'education' => false,
                        'eligibility' => false,
                        'position' => false,
                        'barangay' => false,
                        'religion' => false,
                        'disability' => false,
                    ]
                ]);

                $education = TableRegistry::getTableLocator()->get('Educations')
                    ->find()
                    ->where([
                        'Educations.education like' => '%'.(ucwords($this->request->getData('education'))).'%',
                        'Educations.is_active =' => intval(1),
                    ])
                    ->first();

                if(!empty($education)){
                    $teacher->education_id = intval($education->id);
                }else{
                    $education = TableRegistry::getTableLocator()->get('Educations')
                        ->newEmptyEntity();
                    $education = TableRegistry::getTableLocator()->get('Educations')
                        ->patchEntity($education, [
                            'user_id' => intval(1),
                            'education' => ucwords($this->request->getData('education')),
                            'is_active' => intval(1)
                        ]);
                    if(TableRegistry::getTableLocator()->get('Educations')->save($education)){
                        $teacher->education_id = intval($education->id);
                    }

                }

                $teacher->is_transfered = intval(0);
                $teacher->is_occupied = intval(1);
                $teacher->token = uniqid().microtime();

                if ($this->Teachers->save($teacher)) {
                    $connection->commit();
                    $result = ['message' => ucwords('your application has been submitted'), 'result' => ucwords('success'),
                        'redirect' => Router::url(['prefix' => false, 'controller' => 'Teachers', 'action' => 'view', base64_encode(strval($teacher->id))])];
                    return $this->response->withStatus(200)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }else{
                    foreach ($teacher->getErrors() as $key => $value){
                        $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                            'errors' => $teacher->getErrors()];
                        return $this->response->withStatus(422)->withType('application/json')
                            ->withStringBody(json_encode($result));
                    }
                }
            }catch (\Exception $exception){
                $connection->rollback();
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }

        $employment = $this->Teachers->Employments->find()
            ->contain([
                'TeachingTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'Employments.is_active' => intval(1),
                'Employments.id' => intval($teacher->employment_id)
            ])->firstOrFail();
        $civilStatuses = $this->Teachers->CivilStatuses->find('list', [
                'valueField' => function($query){
                    return ucwords($query->civil_status);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'CivilStatuses.is_active =' => intval(1)
            ])->order(['CivilStatuses.civil_status' => 'ASC'],true);
        $sexes = $this->Teachers->Sexes->find('list', [
                'valueField' => function($query){
                    return ucwords($query->sex);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Sexes.is_active =' => intval(1)
            ])->order(['Sexes.order_position' => 'ASC'],true);
        $genders = $this->Teachers->Genders->find('list', [
                'valueField' => function($query){
                    return ucwords($query->gender);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Genders.is_active =' => intval(1)
            ])->order(['position' => 'ASC'],true);
        $eligibilities = $this->Teachers->Eligibilities->find('list', [
                'valueField' => function($query){
                    return ucwords($query->eligibility);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Eligibilities.is_active =' => intval(1)
            ])->order(['Eligibilities.eligibility' => 'ASC'],true);
        $barangays = $this->Teachers->Barangays->find('list', [
                'valueField' => function($query){
                    return ucwords($query->barangay);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Barangays.is_active =' => intval(1),
                'Barangays.is_defined =' => intval($teacher->is_non_teaching)
            ])->order(['Barangays.barangay' => 'ASC'],true);
        $religions = $this->Teachers->Religions->find('list', [
                'valueField' => function($query){
                    return ucwords($query->religion);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])->where([
                'Religions.is_active =' => intval(1)
            ])->order(['Religions.religion' => 'ASC'],true);
        $disabilities = $this->Teachers->Disabilities->find()
            ->select([
                'id',
                'disability',
                'is_no'
            ])
            ->where([
                'Disabilities.is_active =' => intval(1)
            ])
            ->order(['Disabilities.position' => 'ASC'],true);
        $suffixes = TableRegistry::getTableLocator()->get('Suffixes')
            ->find('list', [
                'valueField' => function($query){
                    return strtoupper($query->suffix);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Suffixes.is_active =' => intval(1)
            ])->order(['Suffixes.order_position' => 'ASC'],true);
        $positions = TableRegistry::getTableLocator()->get('Positions')->find('list', [
                'valueField' => function($query){
                    return ucwords($query->position);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Positions.teaching_type_id =' => intval($teacher->teaching_type_id),
                'Positions.is_active =' => intval(1)
            ])
            ->order(['Positions.position' => 'ASC'],true);
        $this->set(compact('teacher', 'employment', 'civilStatuses', 'sexes', 'genders', 'eligibilities', 'positions', 'barangays', 'religions', 'disabilities', 'suffixes'));

    }

}
