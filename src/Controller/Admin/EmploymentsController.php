<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Moment\Moment;

/**
 * Employments Controller
 *
 * @property \App\Model\Table\EmploymentsTable $Employments
 * @method \App\Model\Entity\Employment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmploymentsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

    }

    public function getEmployments(){
        $data = $this->Employments->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'TeachingTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getEmploymentsDeleted(){
        $data = $this->Employments->find('all',['withDeleted'])
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'TeachingTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->whereNotNull('Employments.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function getEmploymentLists($teachingTypeId = null){
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Employments')
            ->find('list', [
                'valueField' => function($query){
                    return ucwords($query->employment).' | '.($query->teaching_type->teaching_type).' ('.($query->start_date.' - '.$query->end_date).') ';
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->contain([
                'TeachingTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'OR' => [
                    [
                        'Employments.start_date <=' => $now,
                        'Employments.end_date >=' => $now,
                    ],
                    [
                        'Employments.is_per_year =' => intval(1),
                    ],
                ],
                'Employments.is_active =' => intval(1),
                'Employments.teaching_type_id =' => intval($teachingTypeId)
            ])
            ->order([
                'is_per_year' => 'DESC',
                'start_date' => 'ASC'
            ], true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    /**
     * View method
     *
     * @param string|null $id Employment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employment = $this->Employments->get($id);
        $teachingTypes = TableRegistry::getTableLocator()->get('TeachingTypes')
            ->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->teaching_type);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'TeachingTypes.is_active =' => intval(1)
            ])
            ->order([
                'TeachingTypes.teaching_type' => 'ASC'
            ],true);

        $this->set(compact('employment', 'teachingTypes'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employment = $this->Employments->newEmptyEntity();
        if ($this->request->is('post')) {
            $employment = $this->Employments->patchEntity($employment, $this->request->getData());
            $employment->user_id = $this->Auth->user('id');

            $validator = new Validator();

            $validator
                ->add('start_date', 'start_date',[
                    'rule' => function($value){

                        $startDate = (new Moment($value,'Asia/Manila'));
                        $endDate = (new Moment($this->request->getData('end_date'),'Asia/Manila'));

                        if($startDate > $endDate){
                            return ucwords('Start Date Must Be Lower Than End Date');
                        }

                        return true;
                    }
                ]);

            $validator
                ->add('end_date', 'end_date',[
                    'rule' => function($value){

                        $startDate = (new Moment($this->request->getData('start_date'),'Asia/Manila'));
                        $endDate = (new Moment($value,'Asia/Manila'));

                        if($startDate > $endDate){
                            return ucwords('End Date Must Be Higher Than Start Date');
                        }

                        return true;
                    }
                ]);

            $errors = $validator->validate($this->request->getData());
            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            if ($this->Employments->save($employment)) {
                $result = ['message' => ucwords('The employment has been saved'), 'result' => ucwords('success'),
                    'redirect' => Router::url(['prefix' => 'Admin', 'controller' => 'Employments', 'action' => 'index'])];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($employment->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $employment->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $teachingTypes = TableRegistry::getTableLocator()->get('TeachingTypes')
            ->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->teaching_type);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'TeachingTypes.is_active =' => intval(1)
            ])
            ->order([
                'TeachingTypes.teaching_type' => 'ASC'
            ],true);
        $this->set(compact('employment', 'teachingTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Employment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employment = $this->Employments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employment = $this->Employments->patchEntity($employment, $this->request->getData());
            $employment->user_id = $this->Auth->user('id');

            $validator = new Validator();

            $validator
                ->add('start_date', 'start_date',[
                    'rule' => function($value){

                        $startDate = (new Moment($value,'Asia/Manila'));
                        $endDate = (new Moment($this->request->getData('end_date'),'Asia/Manila'));

                        if($startDate > $endDate){
                            return ucwords('Start Date Must Be Lower Than End Date');
                        }

                        return true;
                    }
                ]);

            $validator
                ->add('end_date', 'end_date',[
                    'rule' => function($value){

                        $startDate = (new Moment($this->request->getData('start_date'),'Asia/Manila'));
                        $endDate = (new Moment($value,'Asia/Manila'));

                        if($startDate > $endDate){
                            return ucwords('End Date Must Be Higher Than Start Date');
                        }

                        return true;
                    }
                ]);

            $errors = $validator->validate($this->request->getData());
            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            if ($this->Employments->save($employment)) {
                $result = ['message' => ucwords('The employment has been saved'), 'result' => ucwords('success'),
                    'redirect' => Router::url(['prefix' => 'Admin', 'controller' => 'Employments', 'action' => 'index'])];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($employment->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $employment->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Employment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employment = $this->Employments->get($id);

        $teacher = TableRegistry::getTableLocator()->get('Teachers')
            ->find()
            ->where([
                'employment_id =' => intval($employment->id)
            ])
            ->last();

        if(!empty($teacher)){
            $result = ['message' => ucwords('The employment has been constrained to teachers'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Employments->delete($employment)) {
            $result = ['message' => ucwords('The employment has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The employment has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employment = $this->Employments->get($id,[
            'withDeleted'
        ]);
        if ($this->Employments->restore($employment)) {
            $result = ['message' => ucwords('The employment has been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The employment has not been restored'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employment = $this->Employments->get($id,[
            'withDeleted'
        ]);

        $teacher = TableRegistry::getTableLocator()->get('Teachers')
            ->find()
            ->where([
                'employment_id =' => intval($employment->id)
            ])
            ->last();

        if(!empty($teacher)){
            $result = ['message' => ucwords('The employment has been constrained to teachers'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Employments->hardDelete($employment)) {
            $result = ['message' => ucwords('The employment has been delete'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The employment has not been delete'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

}
