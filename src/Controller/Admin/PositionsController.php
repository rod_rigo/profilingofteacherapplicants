<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Moment\Moment;

/**
 * Positions Controller
 *
 * @property \App\Model\Table\PositionsTable $Positions
 * @method \App\Model\Entity\Position[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PositionsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Positions->newEmptyEntity();
        $teachingTypes = TableRegistry::getTableLocator()->get('TeachingTypes')
            ->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->teaching_type);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'TeachingTypes.is_active =' => intval(1)
            ])
            ->order([
                'TeachingTypes.teaching_type' => 'ASC'
            ],true);
        $this->set(compact('entity', 'teachingTypes'));
    }

    public function getPositions(){
        $data = $this->Positions->find()
            ->contain([
                'Users' => function($query){
                    return $query->find('all',['withDeleted']);
                },
                 'TeachingTypes' => function($query){
                    return $query->find('all',['withDeleted']);
                }
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getPositionsDeleted(){
        $data = $this->Positions->find('all',['withDeleted'])
            ->contain([
                'Users' => function($query){
                    return $query->find('all',['withDeleted']);
                },
                 'TeachingTypes' => function($query){
                    return $query->find('all',['withDeleted']);
                }
            ])
            ->whereNotNull('Positions.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function getPositionLists($teachingTypeId = null){
        $data =  $this->Positions->find('list',[
            'valueField' => function($query){
                return ucwords($query->position);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])
        ->where([
            'Positions.is_active =' => intval(1),
            'Positions.teaching_type_id =' => intval($teachingTypeId)
        ])
        ->order([
            'Positions.position' => 'ASC'
        ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $position = $this->Positions->newEmptyEntity();
        if ($this->request->is('post')) {
            $position = $this->Positions->patchEntity($position, $this->request->getData());
            $position->user_id = $this->Auth->user('id');
            if ($this->Positions->save($position)) {
                $result = ['message' => ucwords('The position has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($position->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $position->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $position = $this->Positions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $position = $this->Positions->patchEntity($position, $this->request->getData());
            $position->user_id = $this->Auth->user('id');
            if ($this->Positions->save($position)) {
                $result = ['message' => ucwords('The position has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($position->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $position->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($position));
    }

    /**
     * Delete method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $position = $this->Positions->get($id);

        $teacher = TableRegistry::getTableLocator()->get('Teachers')
            ->find()
            ->where([
                'position_id =' => intval($position->id)
            ])
            ->last();

        if(!empty($teacher)){
            $result = ['message' => ucwords('The position has been constrained to teachers'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Positions->delete($position)) {
            $result = ['message' => ucwords('The position has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The position has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $position = $this->Positions->get($id,[
            'withDeleted'
        ]);
        if ($this->Positions->restore($position)) {
            $result = ['message' => ucwords('The position has been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The position has not been restored'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $position = $this->Positions->get($id,[
            'withDeleted'
        ]);

        $teacher = TableRegistry::getTableLocator()->get('Teachers')
            ->find()
            ->where([
                'position_id =' => intval($position->id)
            ])
            ->last();

        if(!empty($teacher)){
            $result = ['message' => ucwords('The position has been constrained to teachers'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Positions->hardDelete($position)) {
            $result = ['message' => ucwords('The position has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The position has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

}
