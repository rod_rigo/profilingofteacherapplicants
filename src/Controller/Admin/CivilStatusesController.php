<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * CivilStatuses Controller
 *
 * @property \App\Model\Table\CivilStatusesTable $CivilStatuses
 * @method \App\Model\Entity\CivilStatus[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CivilStatusesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->CivilStatuses->newEmptyEntity();

        $this->set(compact('entity'));
    }

    public function getCivilStatuses(){
        $data = $this->CivilStatuses->find()
            ->contain([
                'Users' => function($query){
                    return $query->find('all',['withDeleted']);
                }
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getCivilStatusesDeleted(){
        $data = $this->CivilStatuses->find('all',['withDeleted'])
            ->contain([
                'Users' => function($query){
                    return $query->find('all',['withDeleted']);
                }
            ])
            ->whereNotNull('CivilStatuses.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $civilStatus = $this->CivilStatuses->newEmptyEntity();
        if ($this->request->is('post')) {
            $civilStatus = $this->CivilStatuses->patchEntity($civilStatus, $this->request->getData());
            $civilStatus->user_id = $this->Auth->user('id');
            if ($this->CivilStatuses->save($civilStatus)) {
                $result = ['message' => ucwords('The civil status has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($civilStatus->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $civilStatus->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $civilStatus = $this->CivilStatuses->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $civilStatus = $this->CivilStatuses->patchEntity($civilStatus, $this->request->getData());
            $civilStatus->user_id = $this->Auth->user('id');
            if ($this->CivilStatuses->save($civilStatus)) {
                $result = ['message' => ucwords('The civil status has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($civilStatus->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $civilStatus->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($civilStatus));
    }

    /**
     * Delete method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $civilStatus = $this->CivilStatuses->get($id);

        $teacher = TableRegistry::getTableLocator()->get('Teachers')
            ->find()
            ->where([
                'civil_status_id =' => intval($civilStatus->id)
            ])
            ->last();

        if(!empty($teacher)){
            $result = ['message' => ucwords('The civil status has been constrained to teachers'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->CivilStatuses->delete($civilStatus)) {
            $result = ['message' => ucwords('The civil status has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The civil status has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $civilStatus = $this->CivilStatuses->get($id,[
            'withDeleted'
        ]);
        if ($this->CivilStatuses->restore($civilStatus)) {
            $result = ['message' => ucwords('The civil status has been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The civil status has not been restored'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $civilStatus = $this->CivilStatuses->get($id,[
            'withDeleted'
        ]);

        $teacher = TableRegistry::getTableLocator()->get('Teachers')
            ->find()
            ->where([
                'civil_status_id =' => intval($civilStatus->id)
            ])
            ->last();

        if(!empty($teacher)){
            $result = ['message' => ucwords('The civil status has been constrained to teachers'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->CivilStatuses->hardDelete($civilStatus)) {
            $result = ['message' => ucwords('The civil status has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The civil status has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }


}
